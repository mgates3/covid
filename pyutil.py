from __future__ import print_function

import re

# import Image
import numpy
from numpy import array, zeros, nonzero

import sys
if (sys.version_info.major >= 3):
	xrange = range

# ------------------------------------------------------------
# constants
MAXINT = int( 2**31-1 )


# # ------------------------------------------------------------
# # Read single grayscale image from file.
# # Returns data in Fortran order, since this is how images are actually stored.
# def read_image( filename ):
# 	img   = Image.open( filename )
# 	bands = img.getbands()
# 	if ('A' in bands and 'L' in bands):
# 		print( 'ignoring alpha channel' )
# 		img = img.convert('L')
# 		bands = img.getbands()
# 	if (bands != ('L',)):
# 		raise Exception('image not grayscale')
# 	data = numpy.fromstring( img.tostring(), numpy.uint8 )
# 	# note that images are really column-major, with x (1st index) increasing fastest.
# 	data = data.reshape( img.size, order='F' )
# 	return data
# # end
#
#
# # ------------------------------------------------------------
# # Write single grayscale image to file.
# def write_image( data, filename ):
# 	# convert to 8-bit
# 	bytes = zeros( data.shape, order='f', dtype=numpy.uint8 )
# 	bytes[:,:] = data.clip( 0, 255 ).round()
#
# 	# L for luminance (gray scale)
# 	img = Image.fromstring( 'L', bytes.shape, bytes.tostring( order='F' ))
# 	img.save( filename )
# # end


# ------------------------------------------------------------
def intersect( A, B ):
	'''
	Finds common elements in A and B
	Returns pair of index arrays (ii, jj) such that a[ii] == b[jj].
	'''
	(ii,) = numpy.where( numpy.in1d( A, B ))
	(jj,) = numpy.where( numpy.in1d( B, A ))
	return (ii, jj)
# end


# ------------------------------------------------------------
def groupby( data, index, reduction=numpy.min ):
	'''
	Returns array with unique values in column index,
	and specified reduction in all other columns, like an SQL groupby query.
	'''
	if (data is None):
		return None

	values = numpy.unique( data[:,index] )
	rows = len(values)
	cols = list( range( data.shape[1] ))
	cols.remove( index )
	data2 = numpy.zeros(( rows, data.shape[1] ))
	for i in xrange( rows ):
		(ii,) = numpy.where( data[:,index] == values[i] )
		data2[i,index] = values[i]
		for j in cols:
			data2[i,j] = reduction( data[ii,j] )
	# end
	return data2
# end


# ------------------------------------------------------------
# adds a multi-column index to a table, to be used for groupby
def add_index( data, indices ):
	(m,n) = data.shape
	data2 = numpy.zeros(( m, n+1 ))
	data2[:,0:n] = data
	widths = numpy.zeros( len( indices ))
	for i in indices:
		widths[i] = numpy.ceil( numpy.log10( numpy.max( numpy.abs( data[:,i] ))))
	# end
	if (sum(widths) > 15):
		print( 'index width too wide, need', sum(widths), 'digits' )
	for i in indices:
		w = 10**widths[i]
		data2[:,n] = data2[:,n]*w + data2[:,i]
	# end
	return data2
# end


# ------------------------------------------------------------
def groupby_cols( data, index, reductions ):
	'''
	Returns array with unique values in column index,
	and specified reductions in all other columns, like an SQL groupby query.
	Reductions is a list of pairs (reduction-function, column-index),
	for example:
		groupby_cols( data, n_idx, ((min,gflop_idx), (std,gflop_idx))
	'''
	if (data is None):
		return None

	n = len( reductions )
	values = numpy.unique( data[:,index] )
	rows = len(values)
	data2 = numpy.zeros(( rows, n+1 ))

	for i in xrange( rows ):
		(ii,) = numpy.where( data[:,index] == values[i] )
		data2[i,0] = values[i]
		for ((reduction, j), j2) in zip( reductions, xrange(1,n+1) ):
			#print( 'j', j, 'j2', j2, 'red', reduction )
			data2[i,j2] = reduction( data[ii,j] )
	# end
	return data2
# end


# ------------------------------------------------------------
def groupby_min( data, index ):
	'''
	Returns array with unique values in column index,
	and min-reduction in all other columns, like an SQL groupby query.
	'''
	if (data is None):
		return None

	values = numpy.unique( data[:,index] )
	rows = len(values)
	cols = data.shape[1]
	data2 = numpy.zeros(( rows, cols ))
	for i in xrange( rows ):
		(ii,) = numpy.where( data[:,index] == values[i] )
		for j in xrange( cols ):
			data2[i,j] = numpy.min( data[ii,j] )
	# end
	return data2
# end


# ------------------------------------------------------------
def groupby_max( data, index ):
	'''
	Returns array with unique values in column index,
	and min-reduction in all other columns, like an SQL groupby query.
	'''
	if (data is None):
		return None

	values = numpy.unique( data[:,index] )
	rows = len(values)
	cols = data.shape[1]
	data2 = numpy.zeros(( rows, cols ))
	for i in xrange( rows ):
		(ii,) = numpy.where( data[:,index] == values[i] )
		for j in xrange( cols ):
			data2[i,j] = numpy.max( data[ii,j] )
	# end
	return data2
# end


# ----------------------------------------
def irange( a, b=None, c=None ):
	'''
	Inclusive range: returns [a, ..., b] inclusive.
	'''
	if (b is None):
		if (type(a) is int):
			a += 1
		else:
			a += 0.01
	else:
		if (c is None):
			if (type(a) is int and type(b) is int):
				b += 1
			else:
				b += 0.01
		else:
			if (type(a) is int and type(b) is int and type(c) is int):
				b += 1
			else:
				b += 0.01*c
			# end
		# end
	# end
	return numpy.arange( a, b, c )
# end


# ----------------------------------------
# Convenience function; sorts array AND returns newly sorted array,
# making this a one-line process.
def sort( array ):
	array.sort()
	return array
# end


# ----------------------------------------
# Print dictionary in 2 column, "key: value" format
# If pattern is given, print only keys matching pattern regular expression.
def print_dictionary( d, pattern=None ):
	keys = sort( d.keys() )
	length = 0
	for key in keys:
		if ( not pattern or re.search( pattern, key )):
			length = max( length, len(key) )
	for key in keys:
		if ( not pattern or re.search( pattern, key )):
			print( '%-*s:' % (length, key), d[key] )
# end


# ----------------------------------------
def print_matrix( A, format='%7.4f' ):
	'''
	Print numpy matrices in more natural ordering and formatting.
	1D arrays are printed as row vectors.
	2D arrays are printed as matrices.
	3D arrays are printed as a series of 2D matrices, e.g.,
	  for k=0, ..., nk
	    print A[:,:,k]
	(numpy does them with i=0, ..., ni, which is very unintuitive to me.)
	Format is any printf-style format string.
	'''
	if (A.ndim == 3):
		for k in xrange( A.shape[2] ):
			for i in xrange( A.shape[0] ):
				for j in xrange( A.shape[1] ):
					print( format % A[i,j,k], end='' )
				print()
			print()
		# end
	elif (A.ndim == 2):
		for i in xrange( A.shape[0] ):
			for j in xrange( A.shape[1] ):
				print( format % A[i,j], end='' )
			print()
	elif (A.ndim == 1):
		for i in xrange( A.shape[0] ):
			print( format % A[i], end='' )
		print()
	else:
		print( A )  # default numpy print
# end


# ----------------------------------------
#              '__  -3.21e+03'  #   big numbers; could use %11.4, shifting decimal point
#              '__-998.654321'  #   most digits
#              '__  -0.000321'  # fewest digits
#              '__   0.      '  #          zero
#              '__  -3.21e-04'  # small numbers; could use %11.4, shifting decimal point
k_int_format = '  %4.0f.      '
k_str_format = '  %11.6f'
k_exp_format = '  %11.2e'
k_str_zero   = '     0.      '
k_small      = 1e-4
k_big        = 1e3

#              '_-876.4321'
#              '_   0.    '
#              '_-1.23e+00'  # minimim size for exponent format
k_int_format = '  %4.0f.    '
k_str_format = '  %9.4f'
k_exp_format = '  %9.2e'
k_str_zero   = '     0.    '
k_small      = 1e-3
k_big        = 1e3

def str_matrix_recursive( A, flt, exp, zero ):
	'''
	Print numpy matrices in more natural ordering and formatting.
	1D arrays are printed as row vectors.
	2D arrays are printed as matrices.
	3D arrays are printed as a series of 2D matrices, e.g.,
	    for k=0, ..., nk
	        print A[:,:,k]
	(numpy prints A[i,:,:] for i=0, ..., ni-1, which is very unintuitive to me.)
	4D arrays are printed as series of 3D arrays, e.g.,
	    for m=0, ..., nm
	        print A[:,:,:,m]
	Format is a printf-style format string that takes a single float.

	flt  is floating point format, e.g., %11.6f, for     1e-4 < x < 1e3.
	exp  is exponential    format, e.g., %11.2e, for x < 1e-4  or   1e3 < x.
	zero is for entries that are exactly zero.
	'''
	res = ''
	if (A.ndim == 4):
		for m in xrange( A.shape[3] ):
			res += str_matrix_recursive( A[:,:,:,m], flt, exp, zero )
			if (m < A.shape[3]-1):
				res += '# -----------'
		# end
	elif (A.ndim == 3):
		for k in xrange( A.shape[2] ):
			res += str_matrix_recursive( A[:,:,k], flt, exp, zero )
		# end
	elif (A.ndim == 2):
		res = '\n'
		for i in xrange( A.shape[0] ):
			for j in xrange( A.shape[1] ):
				if (A[i,j] == numpy.floor(A[i,j])):
					res += k_int_format % A[i,j]
				elif (k_small < abs(A[i,j]) < k_big):
					res += flt % A[i,j]
				else:
					res += exp % A[i,j]
			res += '\n'
		# end
	elif (A.ndim == 1):
		for i in xrange( A.shape[0] ):
			if (A[i] == numpy.floor(A[i])):
				res += k_int_format % A[i]
			elif (k_small < abs(A[i]) < k_big):
				res += flt % A[i]
			else:
				res += exp % A[i]
		# end
		res += '  '
	elif (A.ndim == 0):
		if (A == numpy.floor(A)):
			res += k_int_format % A
		elif (k_small < abs(A) < k_big):
			res += flt % A
		else:
			res += exp % A
	else:
		res += 'str_matrix: unsupported dimensions (ndim=%d)' % A.ndim
	return res
# end

def str_matrix( A, str_format=k_str_format, exp_format=k_exp_format, str_zero=k_str_zero ):
	if (A.ndim == 0):
		res =       str_matrix_recursive( A, str_format, exp_format, str_zero )
	else:
		res = '[' + str_matrix_recursive( A, str_format, exp_format, str_zero ) + ']'
	return res
# end

# called for str(X) or print X
numpy.set_string_function( str_matrix, repr=False )


# ----------------------------------------
# Takes vectors X, Y and reshapes them into the proper 2D grid arrays.
# Returns reshaped (X, Y).

def regrid_2d( X, Y, order='C' ):
	nxy = len( X )

	# find plane X=0
	ii   = nonzero( X == X[0] )
	ny   = len( ii[0] )
	nx   = nxy / ny

	# DVC output files are in Fortran (colwise) order, not C (rowwise) order
	shape = (nx, ny)
	X = X.reshape( shape, order=order )
	Y = Y.reshape( shape, order=order )

	return (X, Y)
# end regrid_2d


# ----------------------------------------
# Takes vectors X, Y, Z and reshapes them into the proper 3D grid arrays.
# Returns reshaped (X, Y, Z).

def regrid_3d( X, Y, Z, order='C' ):
	nxyz = len( X )

	# find plane X=0
	ii   = nonzero( X == X[0] )
	nyz  = len( ii[0] )
	yii  = Y[ii]

	# find line X=0, Y=0
	jj   = nonzero( yii == yii[0] )
	nz   = len( jj[0] )

	ny   = nyz / nz
	nx   = nxyz / nyz

	#step = Z[1] - Z[0]
	#print( 'nx', nx, ny, nz, 'total', nxyz, 'step', step )
	#
	#xmin = X.min()
	#xmax = X.max()
	#ymin = Y.min()
	#ymax = Y.max()
	#zmin = Z.min()
	#zmax = Z.max()
	#
	#print( 'x', xmin, xmax, 'y', ymin, ymax, 'z', zmin, zmax )
	#
	#x = X[0]
	#y = Y[0]
	#z = Z[0]
	#bad = 0
	#for ii in xrange( len( X )):
	#	if (X[ii] != x or Y[ii] != y or Z[ii] != z):
	#		print( 'xyz', x, y, z, 'XYZ[', ii, ']', X[ii], Y[ii], Z[ii] )
	#		x = X[ii]
	#		y = Y[ii]
	#		z = Z[ii]
	#		bad += 1
	#	# end
	#
	#	z += step
	#	if (z > zmax):
	#		z = zmin
	#		y += step
	#		if (y > ymax):
	#			y = ymin
	#			x += step
	#			if (x > xmax and ii != len(X)-1):
	#				raise Exception
	#	# end
	## end
	#print( 'bad', bad )

	shape = (nx, ny, nz)
	X = X.reshape( shape, order=order )
	Y = Y.reshape( shape, order=order )
	Z = Z.reshape( shape, order=order )

	return (X, Y, Z)
# end regrid_3d


# ----------------------------------------
# Takes double-nested list of arrays and concats them into one array.
# 1D arrays, tuples, and lists are assumed to be column vectors.
# If you intend a row vector, make 1xN array, as shown below for y.
# Array dimensions must be compatible. However, scalars assume whatever
# dimensions are imposed by the surrounding blocks, or 1 if none is imposed.
#
# For example:
#   a = 100
#   x = array( [ 1, 3 ] )                     # 2x1 col vector
#   y = array( [[ 10, 20, 30 ]] )             # 1x3 row vector
#
#   B = array( [[ 4, 5, 6 ],
#               [ 7, 8, 9 ]] )  # 2x3 array
#
#   A = concat( [[ a, y, 1 ],
#                [ x, B, 2 ]] )  # 3x5 array
#   print A
#   [[ 100.   10.   20.   30.    1.]
#    [   1.    4.    5.    6.    2.]
#    [   3.    7.    8.    9.    2.]]

def concat( A ):
	m = len( A )
	n = len( A[0] )

	# -----
	# determine rows and cols in each row and col block,
	# and verify that all dimensions are compatible.
	rows = zeros( m, dtype=numpy.int32 )
	cols = zeros( n, dtype=numpy.int32 )
	for i in xrange( m ):
		for j in xrange( n ):
			# convert lists to numpy arrays for easier handling
			Aij = A[i][j]
			if ( type( Aij ) in (list,tuple)):
				Aij = array( Aij )
			# endif

			if (type( Aij ) is numpy.ndarray):
				if (Aij.ndim == 2):
					(mm, nn) = Aij.shape
				elif (Aij.ndim == 1):
					mm = Aij.shape[0]
					nn = 1
				# endif

				if (rows[i] == 0):
					rows[i] = mm
				elif (rows[i] != mm):
					raise Exception( "incompatible number of rows in row block %d" % i )

				if (cols[j] == 0):
					cols[j] = nn
				elif (cols[j] != nn):
					raise Exception( "incompatible number of cols in col block %d" % j )
			# endif
		# end
	# end

	# -----
	# ensure that all row and col block sizes are determined
	# If row or col is all scalars, its size will still be 0, so set it to 1.
	for i in xrange( m ):
		if (rows[i] == 0):
			rows[i] = 1
	# end
	for j in xrange( n ):
		if (cols[j] == 0):
			cols[j] = 1
	# end

	# -----
	# allocate and fill in big array
	nrows = sum( rows )
	ncols = sum( cols )

	AA = zeros( (nrows, ncols) )

	row_start = 0
	for i in xrange( m ):
		row_end = row_start + rows[i]
		col_start = 0
		for j in xrange( n ):
			col_end = col_start + cols[j]

			# convert lists to numpy arrays for easier handling
			Aij = A[i][j]
			if ( type(Aij) in (list,tuple)):
				Aij = array( Aij )

			# force vectors to be column vectors
			if (type(Aij) is numpy.ndarray and Aij.ndim == 1):
				Aij.shape = ( Aij.shape[0], 1 )

			AA[ row_start:row_end, col_start:col_end ] = Aij
			col_start = col_end
		# end
		row_start = row_end
	# end

	return AA
# end


# ----------------------------------------
# formats seconds into nice human readable form, returns string
def format_time( secs ):
	days  = secs  // 86400
	secs -= days  *  86400

	hours = secs  // 3600
	secs -= hours *  3600

	mins  = secs  // 60
	secs -= mins  *  60

	if (days > 0):
		s = '%d days %2d:%02d:%02d' % (days, hours, mins, secs)
	else:
		s = '%2d:%02d:%02d' % (hours, mins, secs)
	return s
# end


# --------------------
def legend( lines=None, labels=None, loc="best", **kwargs ):
	import matplotlib.pyplot as pp
	anchor = None
	locs = re.split( ' +', loc )
	if (len(locs) == 1):
		if   ( locs[0] in ('upper', 'top'   )): locs = ['upper center']
		elif ( locs[0] in ('lower', 'bottom')): locs = ['lower center']
		elif ( locs[0] in ('left'           )): locs = ['center left']
		elif ( locs[0] not in ('right', 'best', 'center')):
			print( 'loc=\'' + loc + '\' is invalid; should be one of:' )
			print( 'best,' )
			print( '                           upper  outside' )
			print( '              upper  left, upper  center (upper), upper  right,' )
			print( 'outside left, center left,        center,         center right, outside right' )
			print( '                (left)                              (right)' )
			print( '              lower  left, lower  center (lower), lower  right,' )
			print( '                           lower  outside' )
			print( '(top=upper and bottom=lower are recognized synonyms)' )
			locs = ['best']
	elif (len(locs) == 2):
		if ( locs[0] in ('left', 'right') or locs[1] in ('lower', 'upper', 'top', 'bottom')):
			locs = [locs[1], locs[0]]  # swap
		if (locs[0] == 'top'   ): locs[0] = 'upper'
		if (locs[0] == 'bottom'): locs[0] = 'lower'
	else:
		raise Exception()

	loc2 = ' '.join( locs )
	if (loc != loc2):
		print( 'using loc=\'' + loc2 + '\'' )

	if (loc2 == 'outside right'):
		anchor = (1.02, 0.5)  # depends on y-axis ticks & labels
		loc2 = 'center left'
	elif (loc2 == 'outside left'):
		anchor = (-0.05, 0.5)  # depends on figure size, which is horrible
		loc2 = 'center right'
	elif (loc2 == 'upper outside'):
		anchor = (0.5, 1)
		loc2 = 'lower center'
	elif (loc2 == 'lower outside'):
		anchor = (0.5, -0.2)  # depends on figure size, which is horrible
		loc2 = 'upper center'
	# end
	#print( 'loc "' + loc2 + '", anchor', anchor )

	# what's the best way to pass these things? this seems overly cumbersome
	if (lines and labels):
		leg = pp.legend( lines, labels, loc=loc2, bbox_to_anchor=anchor, **kwargs )
	elif (lines):
		leg = pp.legend( lines, loc=loc2, bbox_to_anchor=anchor, **kwargs )
	else:
		leg = pp.legend( loc=loc2, bbox_to_anchor=anchor, **kwargs )
	return leg
# end


# --------------------
def plot_fit( fig=None, size=None ):
	import matplotlib.pyplot as pp
	if (fig is None):
		fig = pp.gcf()
	elif (type(fig) in (int,numpy.int32)):
		fig = pp.figure( fig )

# end


# --------------------
def plot_resize( size=None, borders=None, space=None, figures=None, pad=0.5, extra=None ):
	'''
	figurse is figure, number, or list of numbers. If None, the current figure (gcf) is used.
	size    is [ width, height ]
	borders is [ top, right, bottom, left ], as in HTML CSS
	space   is [ wspace, hspace ]
	'''
	import matplotlib.pyplot as pp

	if (figures is None):
		figures = [ pp.gcf() ]
	elif (type(figures) in (int,numpy.int32)):
		figures = [ pp.figure( figures ) ]
	elif (type(figures) in (tuple,list)):
		figures = map( pp.figure, figures )

	for fig in figures:
		if (borders):
			fig.subplots_adjust(
				top    = (1-borders[0]),
				right  = (1-borders[1]),
				bottom =    borders[2],
				left   =    borders[3],
			)
		if (size):
			fig.set_size_inches( size, forward=True )
		if (not borders ):  #and not space):
			fig.canvas.draw()
			fig.tight_layout( pad=pad )
			fig.canvas.draw()
		if (space):
			fig.subplots_adjust(
				wspace = space[0],
				hspace = space[1]
			)
			fig.canvas.draw()
		if (extra):
			t = fig.subplotpars.top
			l = fig.subplotpars.left
			b = fig.subplotpars.bottom
			r = fig.subplotpars.right
			fig.subplots_adjust(
				top    = t - extra[0],
				right  = r - extra[1],
				bottom = b + extra[2],
				left   = l + extra[3],
			)
			fig.canvas.draw()
	# end
	#pp.draw()
# end resize


# --------------------
# left, top is corner of 1st window
# hpad, vpad --- doesn't seem to be doing something sensible
# size sets the window sizes (in inches)
# ncols sets number of columns
def plot_grid( figures, ncols=4, hpad=10, vpad=None, size=None, left=60, top=20, pad=0.5 ):
	import matplotlib.pyplot as pp
	n = len( figures )
	fig = pp.figure( figures[0] )
	if (size):
		fig.set_size_inches( size, forward=True )

	# get size of current window; used to compute position of all windows
	# (even though they may differ in size)
	window = pp.get_current_fig_manager().window
	if ( hasattr( window, 'GetSize' )):      # wx
		(w,h) = window.GetSize()
	elif ( hasattr( window, 'geometry' )):   # Tk
		m = re.search( '^(\d+)x(\d+)\+(\d+)\+(\d+)', window.geometry() )
		w = int( m.group(1) )
		h = int( m.group(2) )
	else:
		print( 'unsupported pyplot window manager' )
	# endif

	if (vpad is None):
		vpad = hpad

	w += hpad
	h += vpad

	# adjust position of all windows
	i = 0
	for f in figures:
		row = i // ncols
		col = i %  ncols
		fig = pp.figure( f )
		window = pp.get_current_fig_manager().window
		x = left + hpad + w*col
		y = top  + vpad + h*row
		if ( hasattr( window, 'SetPosition' )):  # wx
			window.SetPosition( (x, y) )
		elif ( hasattr( window, 'geometry' )):   # Tk
			m = re.search( '^(\d+)x(\d+)\+(\d+)\+(\d+)', window.geometry() )
			w2 = int( m.group(1) )
			h2 = int( m.group(2) )
			window.geometry( '%dx%d+%d+%d' % (w2, h2, x, y) )
		# end

		i += 1
		if (size):
			fig.set_size_inches( size, forward=True )
			fig.canvas.draw()
			fig.tight_layout( pad=pad )
			fig.canvas.draw()
		# end
	# end
# end grid


# --------------------
# makes a stacked bar graph from several rows of data with same x.
# deprecated
def stacked( x, rows, labels=None, colors=None, hatchs=None, edgecolors=None, **args ):
	#print( 'stacked', x, rows, len(rows) )
	import matplotlib.pyplot as pp
	for i in xrange( len( rows )):
		#print( 'i', i )
		# sum all rows after this, to make first row the top row, ..., last row the bottom row.
		# this makes the stack order match the legend order.
		y = numpy.zeros_like( x, dtype=float )
		for j in xrange( i+1, len( rows )):
			y += rows[j]

		row = rows[i]
		if (labels    ): args['label']     = labels[i]
		if (colors    ): args['color']     = colors[i]
		if (edgecolors): args['edgecolor'] = edgecolors[i]
		if (hatchs    ): args['hatch']     = hatchs[i]
		#print( 'bar', x, row, 'bottom', y )
		pp.bar( x, row, bottom=y, **args )
	# end
# end


# --------------------
# Makes a stacked bar graph. Each column of ydata becomes one column (bar).
# Each row becomes one layer or tier, with corresponding label, color,
# edgecolor, and hatch.
def stacked_bar( x, ydata, skipzero=True, labels=None, colors=None, edgecolors=None, hatchs=None, **kwargs ):
	'''
	Makes a stacked bar graph. Each column of ydata becomes one column (bar).
	Each row becomes one layer or tier, with corresponding label, color,
	edgecolor, and hatch.
	# Example:
	# 3 column, 4 row stacked bar graph
	x = [ 0,    1,    2   ]
	y = [
		[ 0.1,  0.2,  0.1 ],  # top row
		[ 0.2,  0.3,  0.3 ],
		[ 0.4,  0.1,  0.4 ],
		[ 0,    0,    0   ],  # all zero row not plotted
		[ 0.3,  0.2,  0.2 ],  # bottom row
	]

	# colors, etc., one for each row
	colors  = [ 'r', 'orange', 'g', 'b', 'm' ]
	hatchs  = [ '/', '//', '\\', '\\\\', 'o' ]
	labels  = [ 'top row', 'second row', 'third row', 'zero row', 'bottom row' ]

	# labels for each column
	xlabels = [ 'column a', 'column b', 'column c' ]

	pp.clf()
	stacked_bar( x, y, colors=colors, hatchs=hatchs, labels=labels, align="center" )
	(xmin, xmax) = pp.xlim()
	pp.xlim(( xmin, xmax+2 ))  # allow space for legend
	pp.xticks( x, xlabels )
	pp.legend( loc='best' )
	'''
	import matplotlib.pyplot as pp

	ydata = numpy.array( ydata, copy=False )  # in case user gave python 2D array
	(m,n) = ydata.shape
	if (len(x) != n):
		raise Exception("error: x and y have different number of columns")

	for i in xrange( m ):
		height = ydata[i,:]
		if (not skipzero or sum(height) != 0):
			bottom = ydata[i+1:,:].sum( axis=0 )
			if (labels     is not None): kwargs['label']     = labels[i]
			if (colors     is not None): kwargs['color']     = colors[i]
			if (edgecolors is not None): kwargs['edgecolor'] = edgecolors[i]
			if (hatchs     is not None): kwargs['hatch']     = hatchs[i]
			pp.bar( x, height, bottom=bottom, **kwargs )
		# end
	# end
# end


# --------------------
# generate labels with "k" suffix to represent 1000's.
def labels_k( ticks ):
	return map( lambda x: '%.0fk' % (x/1000.), ticks )
# end


# --------------------
def savefig( fig, fname ):
	import matplotlib.pyplot as pp
	print( 'saving figure', fig, 'as', fname )
	pp.figure( fig )
	pp.savefig( fname )
# end
