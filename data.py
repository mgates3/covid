import csv
import os
import numpy
import re
import matplotlib
import matplotlib.dates
import datetime

#===============================================================================
# Country, state, and region settings.

# Map: TN => Tennessee
state_name = {
    'AL': 'Alabama',
    'AK': 'Alaska',
    'AZ': 'Arizona',
    'AR': 'Arkansas',
    'CA': 'California',
    'CO': 'Colorado',
    'CT': 'Connecticut',
    'DE': 'Delaware',
    'DC': 'District of Columbia',
    'FL': 'Florida',
    'GA': 'Georgia',
    'HI': 'Hawaii',
    'ID': 'Idaho',
    'IL': 'Illinois',
    'IN': 'Indiana',
    'IA': 'Iowa',
    'KS': 'Kansas',
    'KY': 'Kentucky',
    'LA': 'Louisiana',
    'ME': 'Maine',
    'MD': 'Maryland',
    'MA': 'Massachusetts',
    'MI': 'Michigan',
    'MN': 'Minnesota',
    'MS': 'Mississippi',
    'MO': 'Missouri',
    'MT': 'Montana',
    'NE': 'Nebraska',
    'NV': 'Nevada',
    'NH': 'New Hampshire',
    'NJ': 'New Jersey',
    'NM': 'New Mexico',
    'NY': 'New York',
    'NC': 'North Carolina',
    'ND': 'North Dakota',
    'OH': 'Ohio',
    'OK': 'Oklahoma',
    'OR': 'Oregon',
    'PA': 'Pennsylvania',
    'PR': 'Puerto Rico',
    'RI': 'Rhode Island',
    'SC': 'South Carolina',
    'SD': 'South Dakota',
    'TN': 'Tennessee',
    'TX': 'Texas',
    'UT': 'Utah',
    'VT': 'Vermont',
    'VI': 'Virgin Islands',
    'VA': 'Virginia',
    'WA': 'Washington',
    'WV': 'West Virginia',
    'WI': 'Wisconsin',
    'WY': 'Wyoming',
}

# Construct inverse map: Tennessee => TN
state_abbrev = {}
for st in state_name:
    state_abbrev[ state_name[ st ] ] = st

country_name   = {}  # US => United States
country_abbrev = {}  # United States => US
population     = {}

country_name  [ 'world' ] = 'world'
country_abbrev[ 'world' ] = 'world'
population    [ 'world' ] = 7.8e9

#-------------------------------------------------------------------------------
# Get country names and abbreviations, and country, state, and region populations.
def parse_fips( file ):
    with open( file, newline='' ) as csvfile:
        reader = csv.reader( csvfile )
        header = next( reader )

        # idx_* are column indices in the data files.
        idx_iso2      = None
        idx_region    = None
        idx_state     = None
        idx_country   = None
        idx_pop       = None

        # Parse header.
        j = 0
        for col in header:
            if (re.match('iso2', col)):
                idx_iso2 = j
            elif (re.match('Admin2', col)):
                idx_region = j
            elif (re.search('Province.State', col)):
                idx_state = j
            elif (re.match('Country.Region', col)):
                idx_country = j
            elif (re.match('Population', col)):
                idx_pop = j
            j += 1
        # end header

        print( file, idx_iso2, idx_region, idx_state, idx_country, idx_pop )

        # Parse each data row.
        for row in reader:
            iso2    = row[ idx_iso2 ]
            country = row[ idx_country ]
            state   = row[ idx_state   ]
            region  = row[ idx_region  ]

            country_name  [ iso2    ] = country
            country_abbrev[ country ] = iso2

            if (row[ idx_pop ]):
                key = iso2
                if (state):
                    if (state in state_abbrev):
                        state = state_abbrev[ state ]
                    key += ', ' + state
                if (region):
                    key += ', ' + region

                population[ key ] = float( row[ idx_pop ] )
            # end
        # end row
    # end with
# end

parse_fips( 'COVID-19/csse_covid_19_data/UID_ISO_FIPS_LookUp_Table.csv' )

#===============================================================================
# Time data
start_date = matplotlib.dates.date2num( datetime.date(2020, 1, 22) )

# Maps from country, state, region (county) name to tables.
# Each table has 6 columns:
#     date, confirmed, deaths, recovered, active, tested, hospitalized
nfields = 7
table = {}

# j_* are column indices in the data table.
j = 0
j_date      = j;  j += 1
j_confirmed = j;  j += 1
j_deaths    = j;  j += 1
j_recovered = j;  j += 1
j_active    = j;  j += 1
j_tested    = j;  j += 1
j_hospital  = j;  j += 1

#-------------------------------------------------------------------------------
def parse_report( i, ndates, file, update=False ):
    '''
    i is row in data table (i.e., file index)
    ndates is total number of rows (i.e., number of files)
    file is filename
    update is:
        False for parsing csse_covid_19_daily_reports files
        True  for parsing csse_covid_19_daily_reports_us files
    '''
    if ('world' not in table):
        world_data = numpy.zeros(( ndates, nfields ))
        world_data[:, j_date] = numpy.arange( 0, ndates ) + start_date
        table[ 'world' ] = world_data
    world_data = table[ 'world' ]

    with open( file, newline='' ) as csvfile:
        reader = csv.reader( csvfile )
        header = next( reader )

        # idx_* are column indices in the data files.
        idx_region    = None
        idx_state     = None
        idx_country   = None
        idx_confirmed = None
        idx_deaths    = None
        idx_recovered = None
        idx_active    = None
        idx_tested    = None
        idx_hospital  = None

        # Parse header.
        j = 0
        for col in header:
            if (re.match('Admin2', col)):
                idx_region = j
            elif (re.search('Province.State', col)):
                idx_state = j
            elif (re.match('Country.Region', col)):
                idx_country = j
            elif (re.match('Confirmed', col)):
                idx_confirmed = j
            elif (re.match('Deaths', col)):
                idx_deaths = j
            elif (re.match('Recovered', col)):
                idx_recovered = j
            elif (re.match('Active', col)):
                idx_active = j
            elif (re.match('People_Tested', col)):
                idx_tested = j
            elif (re.match('People_Hospitalized', col)):
                idx_hospital = j
            j += 1
        # end header

        print( i, file,
               idx_region, idx_state, idx_country, idx_confirmed,
               idx_deaths, idx_recovered, idx_active, idx_tested,
               idx_hospital )

        # Parse each data row.
        for row in reader:
            country = row[ idx_country ]
            if (country in country_abbrev):
                country = country_abbrev[ country ]
            if (country not in table):
                country_data = numpy.zeros(( ndates, nfields ))
                country_data[:, j_date] = numpy.arange( 0, ndates ) + start_date
                table[ country ] = country_data
            country_data = table[ country ]

            state = row[ idx_state ]
            if (state in state_abbrev):
                state = state_abbrev[ state ]
            state_key = country + ', ' + state
            if (state_key not in table):
                state_data = numpy.zeros(( ndates, nfields ))
                state_data[:, j_date] = numpy.arange( 0, ndates ) + start_date
                table[ state_key ] = state_data
            state_data = table[ state_key ]

            # Not all data files have idx_region (county)
            if (idx_region is not None):
                # Key like "Knox, TN"
                region = row[ idx_region ]
                region_key = country + ', ' + state + ', ' + region
                if (region_key not in table):
                    region_data = numpy.zeros(( ndates, nfields ))
                    region_data[:, j_date] = numpy.arange( 0, ndates ) + start_date
                    table[ region_key ] = region_data
                region_data = table[ region_key ]
            else:
                region_data = None

            # The order of idx_* columns here must match the order that
            # j_* columns are defined above.
            j = j_confirmed
            for idx in (idx_confirmed, idx_deaths, idx_recovered,
                        idx_active, idx_tested, idx_hospital):
                if (idx and row[ idx ]):
                    value = float( row[ idx ] )
                    if (update):
                        # county data is often lacking recovered, so the active numbers are off;
                        # state data has recovered numbers, hence active numbers.
                        if (state_data[i, j] == 0 or idx == idx_active):
                            world_data  [i, j] += value
                            country_data[i, j] += value
                            state_data  [i, j]  = value
                        elif (state_data[i, j] != value and state == 'AZ'):
                            print( 'discrepency: i %3d, j %2d, %2s, %2s, old %5d, new %5d' %
                                   (i, j, country, state, state_data[i, j], value ) )
                    else:
                        world_data  [i, j] += value
                        country_data[i, j] += value
                        state_data  [i, j] += value
                        if (region_data is not None):
                            region_data[i, j] = value
                # end
                j += 1
            # end idx
        # end row
    # end with
# end parse_report

#-------------------------------------------------------------------------------
print( '-' * 80 )
print( 'Parse daily reports (country, state, and region (county) data)' )
# In the US, these have region (county) level info per row.
# In other countries, these have state/region or country-level info per row.
# State and country information is aggregated from the region info.
# There's only one directory, so this `for` isn't really a loop.
for (root, dirs, files) in os.walk( 'COVID-19/csse_covid_19_data/csse_covid_19_daily_reports' ):
    files = list( filter( lambda x: re.search( '\.csv', x ), files ) )
    files.sort()
    ndates = len( files )
    i = 0
    for file in files:
        file = os.path.join( root, file )
        parse_report( i, ndates, file, update=False )
        i += 1
    # end file
# end walk

#-------------------------------------------------------------------------------
print( '-' * 80 )
print( 'Parse US daily reports (state level data)' )
# These repeat confirmed, deaths, recovered, active (in aggregate form),
# and add people_tested, people_hospitalized (plus some rates).
for (root, dirs, files) in os.walk( 'COVID-19/csse_covid_19_data/csse_covid_19_daily_reports_us' ):
    files = list( filter( lambda x: re.search( '\.csv', x ), files ) )
    files.sort()

    # US daily reports start later than daily reports.
    # Adjust starting row index to match.
    # Better method would be to match dates in filenames.
    n2 = len( files )
    i = ndates - n2

    for file in files:
        file = os.path.join( root, file )
        parse_report( i, ndates, file, update=True )
        i += 1
    # end file
# end walk

#-------------------------------------------------------------------------------
US_NY = table['US'] - table['US, NY']
US_NY[:, j_date] = table['US'][:, j_date]
table['US - NY'] = US_NY

#-------------------------------------------------------------------------------
# Not yet implemented in plots.
smoothing = {
    'US':           (0.013, 0.15),
    'US, TN, Knox': (0.5,   4.0),
}
