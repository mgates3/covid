'''
Plots COVID data from Johns Hopkins:
    https://coronavirus.jhu.edu/map.html
    https://github.com/CSSEGISandData/COVID-19.git

When covid.py is run or imported, it will ask to download the COVID-19
data using git. However, it does not update that data later on. Go into
COVID-19 and do `git pull` to get the latest data.

Uses smoothing splines to give smoothed approximation to the data,
ignoring some of the noise. This is much smoother than the 7-day moving
average that is typical used (also plotted as dashed line).

Limitations:

The slope near the end date is not well determined and is very sensitive to
the smoothing parameter. For regions with few cases or few deaths, or
sudden spikes, the smoothing spline doesn't make much sense. There are
currently no constraints on the spline, so it may overshoot and make
non-physical results such as negative numbers.

The main routine is plot:

    covid.plot( country, state=None, region=None, ... )

Only some countries have state/province level data.
In the U.S., region is county level data.

Example usage:

    import covid

    # Italy
    covid.plot( 'Italy' )

    # Italy, using country code, and overriding smoothing factors.
    covid.plot( 'IT', smooth=0, smooth_cases=5 )

    covid.plot( 'US' )                         # U.S.
    covid.plot( 'US', 'TN' )                   # state of TN
    covid.plot( 'US', 'TN', 'Knox' )           # Knox county
    covid.plot( 'US', 'NY', 'New York City' )  # NYC (all 5 boroughs)

Mark Gates <mgates3@icl.utk.edu>
2020-July
'''
import os
import re
import datetime
import traceback
import subprocess

import numpy
from numpy import log10, log, median, std
from numpy.linalg import norm

import matplotlib
import matplotlib.pyplot as pp
import matplotlib.ticker as ticker

import pyutil
import spline

repo = 'COVID-19'
url  = 'https://github.com/CSSEGISandData/COVID-19.git'
if (not os.path.exists( repo )):
    cmd = ['git', 'clone', url]
    print( 'Data from: ', ' '.join( cmd ) )
    print( 'Download JHU COVID data using git? [Y/n] ', end='' )
    ans = input()
    if (re.search( '^(y|yes)$', ans, re.I )):
        subprocess.run( cmd )
# end

import data
from data import j_date, j_confirmed, j_deaths, j_recovered, \
                 j_active, j_tested, j_hospital

pp.rcParams['axes.axisbelow'] = True

major   = matplotlib.dates.WeekdayLocator( byweekday=matplotlib.dates.SU, interval=2 )
minor   = matplotlib.dates.WeekdayLocator( byweekday=matplotlib.dates.SU )
formatter = matplotlib.dates.DateFormatter( '%b %e' )

# Crop to start 2020-03-01 and end at last date.
us = data.table['US']
start_date   = matplotlib.dates.date2num( datetime.date( 2020, 3, 1 ) )
end_date     = matplotlib.dates.num2date( us[-1, j_date] )
end_date_str = '%02d-%02d' % (end_date.month, end_date.day)

figsize  = [7, 3]
figsize2 = [5, 4]
figsize3 = [7, 4]

color_cases_bar    = '#51BAE7'  # light teal
color_cases        = '#285C72'  # dark teal

color_deaths_bar   = '#EFA63F'  # orange
color_deaths       = '#BF7D37'  # dark orange

color_tests_bar    = '#aa6666'  # lt red
color_tests        = '#aa0000'  # red

color_positive     = '#0000aa'  # blue

color_active_bar   = '#00aa00'  # green
color_active       = '#00aa00'  # green

color_hospital_bar = '#0000aa'  # blue
color_hospital     = '#0000aa'  # blue

#-------------------------------------------------------------------------------
# Use metric G, M, k suffixes. Strip trailing zeros.
def format_metric( x ):
    if (x >= 1e9):
        r = '%.1fG' % (x * 1e-9)
    elif (x >= 1e6):
        r = '%.1fM' % (x * 1e-6)
    elif (x >= 1e3):
        r = '%.1fk' % (x * 1e-3)
    elif (x >= 10):
        r = '%.0f' % (x)
    else:
        r = '%.1f' % (x)
    # Assumes .1f above; will mangle numbers with more precision.
    return re.sub( r'\.0', r'', r )

#-------------------------------------------------------------------------------
def per_100k( x ):
    global population
    return x / (population * 1e-5)

#-------------------------------------------------------------------------------
def per_100k_inverse( x ):
    global population
    return x * (population * 1e-5)

#-------------------------------------------------------------------------------
def labels( legend=True ):
    global population

    axis = pp.gca()
    axis.xaxis.set_major_locator( major )
    axis.xaxis.set_minor_locator( minor )
    axis.xaxis.set_major_formatter( formatter )
    pp.xticks( rotation=30, ha='right' )
    pp.grid( True )
    pp.grid( True, which='minor' )

    ylim = pp.ylim()
    ylim = [ 0, ylim[1] ]
    pp.ylim( ylim )

    xlim = pp.xlim()
    xlim = [ start_date, xlim[1] ]
    pp.xlim( xlim )

    if (population):
        axis.set_ylabel( 'absolute (per 100k)' )
        yticks = axis.get_yticks()
        axis.set_yticks( yticks ) # don't let them change
        axis.set_yticklabels(
            map( lambda x: format_metric( x )
                           + ' (' + format_metric( x / (population*1e-5) ) + ')',
                 yticks ) )
    else:
        axis.set_ylabel( 'absolute' )
        axis.yaxis.set_major_formatter(
            ticker.FuncFormatter( lambda x, _: format_metric( x ) ) )

    # # If population is known, add secondary y-axis with per-capita numbers.
    # if (population):
    #     axis2 = axis.secondary_yaxis( 'right', functions=(per_100k, per_100k_inverse) )
    #     yticks = axis.get_yticks() / (population * 1e-5)
    #     axis2.set_yticks( yticks )
    #     axis2.yaxis.set_major_formatter(
    #         ticker.FuncFormatter( lambda x, _: format_metric( x ) ) )
    #     ylabel = 'per 100,000'
    #     axis2.set_ylabel( ylabel )

    #fig = pp.gcf()
    #fig.legend( loc='center right' )
    # space for legend; but eliminated by tight_layout
    #fig.subplots_adjust( right=0.5 )

    #pp.tight_layout()
    if (legend):
        pyutil.legend( loc='outside right' )  #'upper left' )
    pyutil.plot_resize( figsize )
# end

#-------------------------------------------------------------------------------
def plot( country, state=None, region=None,
          smooth=1,
          smooth_cases=None,
          smooth_deaths=None,
          smooth_tests=None,
          smooth_active=None,
          smooth_hospital=None,
          arrange=2,
          save=False ):
    '''
    region is county in U.S.

    Amount of smoothing is automated based on the 7-day average.
    The smooth* factors multiply the amount of smoothing to fine tune it.
    For instance,
        smooth = 2.0 will do twice the smoothing,
        smooth = 0.5 will do half  the smoothing.
    smooth_{cases, deaths, tests, active, hospital} default to smooth,
    but can be individually overridden. If any smooth_* = 0, its
    smoothing line is disabled.

    save=True will save all plots as PDF in directory by end date.
    '''
    global population

    if (smooth is not None):
        if (smooth_cases    is None): smooth_cases    = smooth
        if (smooth_deaths   is None): smooth_deaths   = smooth
        if (smooth_tests    is None): smooth_tests    = smooth
        if (smooth_active   is None): smooth_active   = smooth
        if (smooth_hospital is None): smooth_hospital = smooth
    if (smooth_cases    is None): smooth_cases    = 0
    if (smooth_deaths   is None): smooth_deaths   = 0
    if (smooth_tests    is None): smooth_tests    = 0
    if (smooth_active   is None): smooth_active   = 0
    if (smooth_hospital is None): smooth_hospital = 0

    print( 'smooth            =', smooth          )
    print( '  smooth_cases    =', smooth_cases    )
    print( '  smooth_deaths   =', smooth_deaths   )
    print( '  smooth_tests    =', smooth_tests    )
    print( '  smooth_active   =', smooth_active   )
    print( '  smooth_hospital =', smooth_hospital )

    #----------------------------------------
    # Get key (country, state, region), title, population, etc.
    try:
        if (country in data.country_abbrev):
            country = data.country_abbrev[ country ]
        key   = country
        title = data.country_name[ country ]

        if (state in data.state_abbrev):
            state = data.state_abbrev[ state ]
        if (state):
            key   += ', ' + state
            title += ', ' + data.state_name[ state ]

        if (region):
            key   += ', ' + region
            title += ', ' + region

        if (key in data.population):
            population = data.population[ key ]
            title += ' (pop. ' + format_metric( population ) + ')'
        else:
            population = None

        tbl = data.table[ key ]
    except Exception as ex:
        traceback.print_exc()
        return
    n = tbl.shape[0]

    #----------------------------------------
    # Lookup data and smooth it.
    global dates, cases, deaths, tests, active, recovered, hospital
    dates     = tbl[:, j_date     ]
    cases     = tbl[:, j_confirmed]
    deaths    = tbl[:, j_deaths   ]
    tests     = tbl[:, j_tested   ]
    active    = tbl[:, j_active   ]
    recovered = tbl[:, j_recovered]
    hospital  = tbl[:, j_hospital ]

    has_tests    = any( tests     )
    has_active   = any( recovered )  # otherwise, active = confirmed - deaths
    has_hospital = any( hospital  )

    # Shift dates by -3.5 days to align 7-day average with mid-point of span.
    global dates_avg, cases_avg, deaths_avg, tests_avg, active_avg, hospital_avg
    span = 7
    dates_avg    =     dates[span:n] - span/2
    cases_avg    = (   cases[span:n] -    cases[0:n-span]) / span
    deaths_avg   = (  deaths[span:n] -   deaths[0:n-span]) / span
    tests_avg    = (   tests[span:n] -    tests[0:n-span]) / span
    active_avg   = (  active[span:n] -   active[0:n-span]) / span
    hospital_avg = (hospital[span:n] - hospital[0:n-span]) / span

    # Daily differences
    global ddates, dcases, ddeaths, dtests
    ddates    =    dates[1:n]
    dcases    =    cases[1:n] -    cases[0:n-1]
    ddeaths   =   deaths[1:n] -   deaths[0:n-1]
    dtests    =    tests[1:n] -    tests[0:n-1]
    dactive   =   active[1:n] -   active[0:n-1]
    dhospital = hospital[1:n] - hospital[0:n-1]

    # Figure difference compared to 7-day average for spline smoothing.
    cases_diff    =    dcases[ 2 : n - 5 ] -    cases_avg
    deaths_diff   =   ddeaths[ 2 : n - 5 ] -   deaths_avg
    tests_diff    =    dtests[ 2 : n - 5 ] -    tests_avg
    active_diff   =   dactive[ 2 : n - 5 ] -   active_avg
    hospital_diff = dhospital[ 2 : n - 5 ] - hospital_avg

    # Dierckx suggests S = delta^2 n
    # for n values, delta is error estimate of each value.
    n = len( dates )
    s_cases    = n * std(    cases_diff )**2 * smooth_cases
    s_deaths   = n * std(   deaths_diff )**2 * smooth_deaths
    s_tests    = n * std(    tests_diff )**2 * smooth_tests
    s_active   = n * std(   active_diff )**2 * smooth_active
    s_hospital = n * std( hospital_diff )**2 * smooth_hospital

    global cases_sp, deaths_sp, tests_sp, active_sp, hospital_sp, dates_sp
    cases_sp    = spline.Spline( dates, cases,    smooth=s_cases    )
    deaths_sp   = spline.Spline( dates, deaths,   smooth=s_deaths   )
    tests_sp    = spline.Spline( dates, tests,    smooth=s_tests    )
    active_sp   = spline.Spline( dates, active,   smooth=s_active   )
    hospital_sp = spline.Spline( dates, hospital, smooth=s_hospital )

    dates_sp = dates + 0.5  # add half day to align spline with bar graphs

    ncases_label = (format( cases[-1],  ',.0f' ) + ' cases; '
                 +  format( deaths[-1], ',.0f' ) + ' deaths'
                 +  '; %.1f%% CFR' % (deaths[-1] / cases[-1] * 100))

    fig = 1

    #----------------------------------------
    # Total tests
    pp.figure( fig ); fig += 1
    pp.clf()
    if (has_tests):
        pp.plot( dates, tests, '--',
                 color=color_tests, label='tests' )
        if (smooth_tests):
            pp.plot( dates_sp, tests_sp(dates), '-',
                     color=color_tests, label='tests, smoothed' )
        pp.title( title + ' cummulative tests\n' + ncases_label )
        labels()

    #----------
    # Daily tests
    pp.figure( fig ); fig += 1
    pp.clf()
    if (has_tests):
        pp.bar( ddates, dtests,
                color=color_tests_bar, label='tests', width=1, align='edge' )
        pp.plot( dates_avg, tests_avg, '--',
                 color=color_tests, label='tests, 7-day avg' )
        if (smooth_tests):
            # For daily splines, take 1st derivative: sp(x, 1)
            pp.plot( dates_sp, tests_sp(dates, 1), '-',
                     color=color_tests, label='tests, smoothed'  )

        # For ymax, ignore first non-zero day, which is huge due to weird data.
        (ii,) = numpy.where( tests > 0 )
        ymax = max( dtests[ ii[ 1 : len(ii)-1 ] ] )
        pp.ylim( [0, ymax] )

        # Compute positive = cases / tests, and filter out garbage.
        (ii_avg,) = numpy.where( tests[span:n] >= 1 )
        positive_avg = cases_avg[ ii_avg ] / tests_avg[ ii_avg ]

        global dtests_sp, dcases_sp, positive
        dtests_sp = tests_sp( dates[ ii ], 1 )
        dcases_sp = cases_sp( dates[ ii ], 1 )
        positive_sp = dcases_sp / dtests_sp

        axis1 = pp.gca()
        axis2 = axis1.twinx()
        axis2.plot( dates_avg[ ii_avg ], positive_avg * 100, '--',
                    color=color_positive, label='% positive, 7-day avg' )
        if (smooth_tests and smooth_cases):
            axis2.plot( dates_sp[ ii ], positive_sp * 100, '-',
                        color=color_positive, label='% positive, smoothed' )
        axis2.tick_params( axis='y', colors=color_positive )
        axis2.yaxis.label.set_color( color_positive )
        axis2.set_ylim( [0, 100] )
        axis2.set_ylabel( '% positive' )
        pp.sca( axis1 ) # reset current axis

        pp.title( title + ' daily tests\n' + ncases_label )
        labels( legend=False )
        f = pp.gcf()
        f.legend( loc='center right' )
        f.subplots_adjust( right=0.6 )

    #----------------------------------------
    # Total cases
    pp.figure( fig ); fig += 1
    pp.clf()
    if (True):
        pp.plot( dates, cases, '--',
                 color=color_cases, label='confirmed cases' )
        if (smooth_cases):
            pp.plot( dates_sp, cases_sp(dates), '-',
                     color=color_cases, label='confirmed cases, smoothed' )

        # Active cases
        if (has_active):
            pp.plot( dates, active, '--',
                     color=color_active, label='active cases' )
            if (smooth_active):
                pp.plot( dates_sp, active_sp(dates), '-',
                         color=color_active, label='active cases, smoothed' )
        pp.title( title + ' cummulative cases\n' + ncases_label )
        labels()

    #----------
    # Daily cases
    # Skip daily change to active, which isn't very useful.
    pp.figure( fig ); fig += 1
    pp.clf()
    if (True):
        pp.bar( ddates, dcases,
                color=color_cases_bar, label='cases', width=1, align='edge' )
        pp.plot( dates_avg, cases_avg, '--',
                 color=color_cases, label='cases, 7-day avg' )
        if (smooth_cases):
            pp.plot( dates_sp, cases_sp(dates, 1), '-',
                     color=color_cases, label='cases, smoothed'  )
        pp.title( title + ' daily cases\n' + ncases_label )
        labels()

    #----------------------------------------
    # Total deaths and hospitalized
    pp.figure( fig ); fig += 1
    pp.clf()
    if (True):
        if (has_hospital):
            pp.plot( dates, hospital, '--',
                     color=color_hospital, label='hospitalized' )
            if (smooth_hospital):
                pp.plot( dates_sp, hospital_sp(dates), '-',
                         color=color_hospital, label='hospitalized, smoothed' )

        # Deaths
        pp.plot( dates, deaths, '--',
                 color=color_deaths, label='deaths' )
        if (smooth_deaths):
            pp.plot( dates_sp, deaths_sp(dates), '-',
                     color=color_deaths, label='deaths, smoothed' )
        pp.title( title + ' cummulative deaths\n' + ncases_label )
        labels()

    #----------
    # Daily deaths
    # Skip daily change to hospitalized, which isn't very useful.
    pp.figure( fig ); fig += 1
    pp.clf()
    if (True):
        pp.bar( ddates, ddeaths,
                color=color_deaths_bar, label='deaths', width=1, align='edge' )
        pp.plot( dates_avg, deaths_avg, '--',
                 color=color_deaths, label='deaths, 7-day avg' )
        if (smooth_deaths):
            pp.plot( dates_sp, deaths_sp(dates, 1), '-',
                     color=color_deaths, label='deaths, smoothed' )

        pp.title( title + ' daily deaths\n' + ncases_label )
        labels()

    #----------------------------------------
    # Risk
    pp.figure( fig ); fig += 1
    pp.clf()
    if (population):
        infections = cases[-1] - cases[-15]
        n = numpy.logspace( 0, 4, 100 )
        risk   = 1 - (1 - infections / population)**n
        risk2  = 1 - (1 - 2*infections / population)**n
        risk5  = 1 - (1 - 5*infections / population)**n
        risk10 = 1 - (1 - 10*infections / population)**n
        pp.semilogx( n, risk10*100, label='$10 i_0 = $' + format_metric( 10*infections ) )
        pp.semilogx( n, risk5 *100, label='$5 i_0  = $' + format_metric( 5*infections ) )
        pp.semilogx( n, risk2 *100, label='$2 i_0  = $' + format_metric( 2*infections ) )
        pp.semilogx( n, risk  *100, label='$i_0    = $' + format_metric( infections ) )

        # non-scientific notation
        pp.gca().xaxis.set_major_formatter(
            ticker.FuncFormatter(lambda x, _: '{:g}'.format(x)))

        pp.title( title + ' risk at event,\n'
                + 'assuming prevalence %.2f%% $\\approx$ 1 in %.0f people\n'
                    % (infections / population * 100, population / infections)
                + 'based on ' + format_metric( infections )  # format( int(infections), ",d" )
                + ' cases in last 2 weeks\n' )
        pp.ylabel( 'percent chance = $1 - \\left( 1 - \\frac{infections}{population} \\right)^n$' )
        pp.xlabel( '$n$: # people at event' )
        pp.grid( True )
        pp.legend( loc='best' )
        pyutil.plot_resize( figsize2 )
        pp.tight_layout()  # shouldn't be needed?

    #----------------------------------------
    # Risk
    pp.figure( fig ); fig += 1
    pp.clf()
    if (population):
        pp.gca().set_axisbelow( False )

        #risk_values = [ 0.01, 0.02, 0.05, 0.10, 0.20, 0.4, 0.6, 0.8, 0.95, 0.99, 0.9999999 ]
        risk_values = [ 0.01, 0.05, 0.20, 0.40, 0.60, 0.80, 0.99, 0.9999999 ]
        risk_values.reverse()

        # event size
        n = numpy.logspace( 0, 4, 100 )

        #global handles
        #handles = []
        nrisk = len( risk_values )
        cmap = matplotlib.cm.jet_r  # hot_r  # autumn # YlOrRd  # Reds
        last_cases = None
        last_risk = 1
        i = 0
        for risk in risk_values:
            cases = (1 - (1 - risk)**(1 / n)) * population
            if (last_cases is not None):
                #label = '%.0f - %.0f%%' % (risk*100, last_risk*100)
                label = '> %.0f%%' % (risk*100)
                pp.fill_between( n, cases, last_cases, label=label, color=cmap( i / nrisk ) )
            last_cases = cases
            last_risk = risk
            i += 1
            if (i == nrisk//2):  # middle color (bright green) isn't great, so skip it
                i += 1
        # end
        label = '< %.0f%%' % (last_risk*100)
        pp.fill_between( n, last_cases, 0, label=label, color='gray' )
        ax = pp.gca()
        ax.set_xscale( 'log' )
        ax.set_yscale( 'log' )
        pp.ylim( [10, population] )

        pp.loglog( [n[0], n[-1]], [10*infections, 10*infections], '-', label='$10 i_0 = $' + format_metric( 10*infections ) )
        pp.loglog( [n[0], n[-1]], [ 5*infections,  5*infections], '-', label='$5 i_0  = $' + format_metric(  5*infections ) )
        pp.loglog( [n[0], n[-1]], [ 2*infections,  2*infections], '-', label='$2 i_0  = $' + format_metric(  2*infections ) )
        pp.loglog( [n[0], n[-1]], [   infections,    infections], '-', label='$i_0    = $' + format_metric(    infections ) + ' infections\nin last 2 weeks' )

        # non-scientific notation
        pp.gca().xaxis.set_major_formatter(
            ticker.FuncFormatter(lambda x, _: '{:g}'.format(x)))
        pp.gca().yaxis.set_major_formatter(
            ticker.FuncFormatter(lambda x, _: '{:g}'.format(x)))

        #handles.reverse()
        pyutil.legend( loc='outside right' )
        pp.title( title + ' risk at event\n'
                  + 'chance = $1 - \\left( 1 - \\frac{infections}{population} \\right)^n$' )
        pp.ylabel( '$i$: # circulating infections' )
        pp.xlabel( '$n$: # people at event' )
        pp.grid( axis='x' )
        pyutil.plot_resize( figsize3 )
        pp.tight_layout()  # shouldn't be needed?

    #----------------------------------------
    # Arrange plots in multi-column grid on screen.
    if (arrange):
        pyutil.plot_grid( range( 1, fig ), arrange )

    #----------------------------------------
    # Save PDFs.
    if (save):
        file = re.sub( ', ', '-', key )
        print( 'saving', os.path.join( end_date_str, '*', file ) )
        fig = 1
        for title in ('total-tests', 'daily-tests',
                      'total-cases', 'daily-cases',
                      'total-deaths', 'daily-deaths',
                      'risk', 'risk-all'):
            os.makedirs( os.path.join( end_date_str, title ), exist_ok=True )
            pp.figure( fig );
            pp.savefig( os.path.join( end_date_str, title, file + '-' + title + '.pdf' ))
            fig += 1
        # end
    # end
# end


#-------------------------------------------------------------------------------
def plot_states( smooth=0, smooth_deaths=0, save=True ):
    '''
    Plots the U.S. and all U.S. states and territories.
    '''
    plot( 'US', smooth=smooth, smooth_deaths=smooth_deaths, save=save )
    for state in data.state_name.keys():
        plot( 'US', state, smooth=smooth, smooth_deaths=smooth_deaths, save=save )
# end
