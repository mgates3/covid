#
# Mark Gates
# mgates17@gmail.com
# Copyright 2018
#
# This software is provided under terms of the Creative Commons
# Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license.
# For a description of the license and its legal terms, see:
# https://creativecommons.org/licenses/by-sa/4.0/
#
# Please refer any questions to the author.
#

# ------------------------------------------------------------------------------
# Implements 1D, 2D, and 3D splines of arbitrary degree.
# Also has specialized versions for cubic splines with uniform, unit-spaced knots.
# Being in Python, this code is EXPECTED TO BE SLOW.
# There are many optimizations to be made in a high-performance solution
# such as in C, C++, or Fortran.
# Also, this code has received only light testing (see _test.py files). It's
# very likely there are some minor, or even major, issues.
#

import numpy
from numpy import inf, dot
from numpy.linalg import norm

import random

# ==============================================================================
# common spline utilities

# ------------------------------------------------------------------------------
def interval( x, tx ):
    '''
    Determines which (non-empty) knot interval the point x is in.

    @param[in] x
        Data point to find.

    @param[in] tx
        Array of knots, of length n.

    @return
        Index i in [0, n-1) such that tx[i] <= x <= tx[i+1], and tx[i] < tx[i+1].
        x == tx[i+1] only if x == tx[n-1].
    '''
    n = len( tx )
    assert( tx[0] <= x )
    assert( x <= tx[n-1] )

    # Find first i such that tx[i] > x, or n-1 if no such knot exists.
    # See C++ std::upper_bound.
    i = tx.searchsorted( x, 'right' ) - 1
    if (i == n - 1):
        # If i is at the end, then find first i such that tx[i] >= x.
        # This finds the non-empty interval before any empty intervals due to
        # coincident knots at the end.
        # See C++ std::lower_bound.
        i = tx.searchsorted( x, 'left' ) - 1
    # end

    assert( 0 <= i and i < n-1 )
    assert( tx[i] <= x and x <= tx[i+1] )
    assert( tx[i] < tx[i+1] )
    return i
# end

# ------------------------------------------------------------------------------
def eval_basis( x, tx, degree, dx=0 ):
    '''
    Evaluate b-spline basis functions with support at point x.

    @param[in] x
        Data point to evaluate.

    @param[in] tx
        Array of knots, of length n.

    @param[in] degree
        Degree of the spline.

    @param[in] dx
        Derivative to evaluate. Default is 0, i.e., the spline itself.

    @return
        Tuple (Ni_x, index), where:
        - Ni_x is an array of length degree + 1 with the basis functions, Ni,
          evaluated at x.
        - index is as in interval()
    '''
    ix = interval( x, tx )
    Ni_x = numpy.zeros(( degree + 1 ))
    if (dx > degree):
        # Derivatives > degree are zero (almost everywhere; delta functions at knots)
        Ni_x[degree] = 0
    else:
        # Compute basis functions Ni_x{i, k+1} up to k=degree.
        # Starting with Ni_x{i, 1} = 1, this takes each known basis function and
        # adds its contribution to the basis functions of next higher degree,
        # e.g., Ni_x{i-1, 2} and Ni_x{i, 2}.
        Ni_x[degree] = 1
        for k in range( 0, degree - dx ):  # for cubics, k = 0, 1, 2
            for j in range( degree - k, degree + 1 ):  # for k = 2, j = degree - 2, degree - 1, degree
                i = ix - degree + j
                Nj_dt = Ni_x[j] / (tx[i + k + 1] - tx[i])
                Ni_x[j - 1] += (tx[i + k + 1] - x)*Nj_dt
                Ni_x[j]      = (x - tx[i])*Nj_dt
            # end
        # end

        # Difference basis functions to get derivatives.
        for k in range( degree - dx, degree ):
            for j in range( degree - k, degree + 1 ):
                i = ix - degree + j
                kNj_dt = (k + 1) * Ni_x[j] / (tx[i + k + 1] - tx[i])
                Ni_x[j - 1] -= kNj_dt
                Ni_x[j]      = kNj_dt
            # end
        # end
    # end

    return (Ni_x, ix)
# end

# ==============================================================================
# boundary conditions

# ------------------------------------------------------------------------------
def derivative_bc_knots( xx, degree ):
    '''
    Determines set of knots when using not-a-knot boundary condition.

    @param[in] xx
        Array of data points, of length n.

    @param[in] degree
        Degree of the spline.

    @return
        Array of knot points, of length n + 2*degree.
    '''
    nx = len(xx)
    assert( nx >= 2 )
    tx = numpy.zeros(( nx + 2*degree ))
    tx[ 0           : degree        ] = xx[0]       # coincident boundary knots
    tx[ degree      : nx + degree   ] = xx          # data points
    tx[ nx + degree : nx + 2*degree ] = xx[nx - 1]  # coincident boundary knots
    return tx
# end

# ------------------------------------------------------------------------------
def derivative_bc_evalE( xx, tx, degree ):
    '''
    Generates the matrix E that enforces interpolation at data points.
    To determine the spline coefficients $C$, solve
        \[ E C = F \],
    where $F$ is function values corresponding to data points in $x$.

    @param[in] xx
        Array of data points, of length n.

    @param[in] tx
        Array of knots, of length m.
        For interpolating splines, m = n + 2*degree.

    @param[in] degree
        Degree of the spline.

    @return
        Matrix E, of size (n + degree - 1)-by-(m - (degree + 1)).
        For interpolating splines, this is square,
        (n + degree - 1)-by-(n + degree - 1).
    '''
    nx = len( xx )
    nbasis = len( tx ) - (degree + 1)  # number of basis functions

    # Number of boundary conditions on each side (left and right).
    b = (degree - 1)//2

    E = numpy.zeros(( nx + degree - 1, nbasis ))

    # Interpolate derivatives at left boundary.
    # Spline here is left-continuous and Ni_x[degree] == 0
    for i in range( b ):
        (Ni_x, ix) = eval_basis( xx[0], tx, degree, b - i )  # (b-i)th derivative
        assert( ix == degree )
        assert( Ni_x[degree] == 0 )
        for j in range( degree + 1 ):
            E[ i, ix - degree + j ] = Ni_x[j]
        # end
    # end

    # Interpolate at data points.
    # Spline is left-continuous except at last data point,
    # so this should never index out-of-bounds.
    for i in range( nx ):
        (Ni_x, ix) = eval_basis( xx[i], tx, degree )
        for j in range( degree + 1 ):
            E[ i + b, ix - degree + j ] = Ni_x[j]
        # end
    # end

    # Interpolate derivatives at right boundary.
    # Spline here is right-continuous and Ni_x[0] == 0.
    for i in range( b ):
        (Ni_x, ix) = eval_basis( xx[nx - 1], tx, degree, i + 1 )  # i-th derivative
        assert( ix == nbasis - 1 )
        assert( Ni_x[0] == 0 )
        for j in range( degree + 1 ):
            E[ i + nx + b, ix - degree + j ] = Ni_x[j]
        # end
    # end

    return E
# end

# ------------------------------------------------------------------------------
def notaknot_bc_knots( xx, degree ):
    '''
    Determines set of knots when using not-a-knot boundary condition.

    @param[in] xx
        Array of data points, of length n.

    @param[in] degree
        Degree of the spline.

    @return
        Array of knot points, of length n + degree + 1.
    '''
    n = len(xx)
    assert( n >= degree + 1 )

    # Number of boundary conditions on each side (left and right).
    b = (degree - 1)//2

    # Add 2*degree boundary knots,
    # but leave out (degree - 1) not-a-knot data points.
    #print( 'degree', degree, 'n', n, 'b', b )

    tx = numpy.zeros(( n + 2*degree - (degree - 1) ))
    tx[ 0 : degree + 1 ]     = xx[ 0 ]                  # coincident boundary knots
    tx[ degree + 1 : n ]     = xx[ b + 1 : n - b - 1 ]  # interior, omitting not-a-knots
    tx[ n : n + degree + 1 ] = xx[ n - 1 ]              # coincident boundary knots
    return tx
# end

# ------------------------------------------------------------------------------
def notaknot_bc_evalE( xx, tx, degree ):
    '''
    Generates the matrix E that enforces interpolation at data points.
    To determine the spline coefficients $C$, solve
        \[ E C = F \],
    where $F$ is function values corresponding to data points in $x$.

    @param[in] xx
        Array of data points, of length n.

    @param[in] tx
        Array of knots, of length m.
        For interpolating splines, m = n + degree + 1.

    @param[in] degree
        Degree of the spline.

    @return
        Matrix E, of size n-by-(m - (degree + 1)).
        For interpolating splines, this is square, n-by-n.
    '''
    nx = len(xx)
    nbasis = len(tx) - (degree + 1)  # number of basis functions
    assert( nx >= nbasis )

    E = numpy.zeros(( nx, nbasis ))
    for i in range( 0, nx ):
        (Ni_x, ix) = eval_basis( xx[i], tx, degree )
        assert( degree <= ix and ix < nbasis )
        for j in range( 0, degree + 1 ):
            E[ i, ix - degree + j ] = Ni_x[j]
        # end
    # end
    return E
# end

# ------------------------------------------------------------------------------
# returns tx
def only_boundary_knots( xx, deg ):
    nx = len(xx)
    if (nx < 2):
        raise Exception( "requires at least 2 data points, got n=" + str(nx) )

    # add 2*deg boundary knots, but leave out (deg-1) not-a-knot data points
    tx = numpy.zeros( [2*deg + 2] )
    tx[ 0       :   deg + 1 ] = xx[ 0 ]        # coincident boundary knots
    tx[ deg + 1 : 2*deg + 2 ] = xx[ nx - 1 ]   # coincident boundary knots
    return tx
# end only_boundary_knots


# ------------------------------------------------------------------------------
# Evaluate jump in 3rd derivative of each basis function at all interior knots.
# Each column corresponds to one of the g+4 basis functions.
# Each row    corresponds to one of the g   interior knots.
# There are g+8 knots total, with 6 boundary knots and 2 end points.
#
# H = [ a{-k,1} ... a{g,1} ]
#     [    .          .    ]
#     [ a{-k,g} ... a{g,g} ]
#
# a{i,q} = (-1)^{k+1} k! (tx{i+k+1} - tx{i})
#            / prod_{j=i to i+k+1, j != q} (tx{q} - tx{j})
# if q-k-1 <= i <= q
#
# returns H
def evalH( tx, deg ):
    if (deg != 3):
        raise Exception("smoothing splines implemented only for deg=3")

    g = len(tx) - 8
    H = numpy.zeros(( g, g + 4 ))
    for i in range( 0, g+4 ):
        for q in range( 4, g+4 ):
            if (q-4 <= i and i <= q):
                d = 1.0
                for j in range( i, i+4+1 ):
                    if (j != q):
                        d *= tx[q] - tx[j]
                # end
                H[ q - 4, i ] = 6.0 * (tx[i+4] - tx[i]) / d
            # endif
        # end
    # end
    return H
# end evalH


# ------------------------------------------------------------------------------
# constants
inf = float('inf')
max_iters = 20
spline_tol = 0.01

# ------------------------------------------------------------------------------
# Compute first p2 from Sp1 (p1=0) and Sp3 (p3=inf).
def first_p( Sp1, Sp3, S ):
    return -( Sp1 - S ) / ( Sp3 - S )

# ------------------------------------------------------------------------------
# Compute next p and replace either (p1,Sp1) or (p3,Sp3) with (p2,Sp2).
# Saves next p in p2. Sp2 is not changed.

def next_p( Sp1, Sp2, Sp3, p1, p2, p3, S ):
	if (p3 == inf):
		p = ( ( p1*(Sp1 - Sp3)*(Sp2 - S)
		      - p2*(Sp2 - Sp3)*(Sp1 - S) )
		    / ( (Sp1 - Sp2)*(Sp3 - S) ))
	else:
		p = -(( (Sp3 - S)*(Sp1 - Sp2)*p1*p2 +
		        (Sp2 - S)*(Sp3 - Sp1)*p3*p1 +
		        (Sp1 - S)*(Sp2 - Sp3)*p2*p3 )
		   /  ( (Sp3 - S)*(Sp1 - Sp2)*p3 +
		        (Sp2 - S)*(Sp3 - Sp1)*p2 +
		        (Sp1 - S)*(Sp2 - Sp3)*p1 ))

	if (Sp2 < S):
		p3  = p2
		Sp3 = Sp2
	else:
		p1  = p2
		Sp1 = Sp2
	p2 = p

	assert( p1 < p2 and p2 < p3 )

	return (Sp1, p1, Sp3, p3, p2)
# end

# ==============================================================================
class Spline:
    # --------------------------------------------------------------------------
    def __init__( self, xx, F, degree=3, smooth=0 ):
        '''
        Constructs an interpolating or smoothing spline of the given degree.

        @param[in] xx
            Array of data points, of length n.

        @param[in] F
            Array of data values.
            For not-a-knot boundary condition, of length n.
            For derivative boundary condition, of length n + degree - 1.

        @param[in] degree
            Degree of the spline. Default is 3, cubic. degree >= 1 and odd.

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        A spline is a piece-wise polynomial function. All or some of the data
        points are specified as knots. Between each pair of knots, the spline
        is a polynomial of the given degree. At each knot, the spline has degree-1
        continuity. Hence, a cubic (degree=3) spline is C^2 continuous.

        A spline of the given degree with n knots has (n-1)*(degree+1) degrees-of-freedom.
        (n-1)degree + (n-1) = nd + n - degree - 1

        n data points, (n-2)*degree enforced continuity at interior knots
        n + (nd - 2d) = nd + n - 2d + (degree-1)
        need (degree-1) extra derivatives.

        Two kinds of boundary conditions are supported:
        - Not-a-knot: the first and last (degree - 1)/2 interior data points
          (i.e., excluding the xx[0] and xx[n-1] endpoints) are not taken as
          knots between polynomials, though their data values are still
          interpolated. That is, the first polynomial spans xx[0] to xx[(degree
          - 1)/2 + 1], and the last polynomial spans xx[n - (degree - 1)/2 - 2]
          to xx[n-1]. For a cubic spline (degree=3), they span xx[0] to xx[2]
          and xx[n-3] to xx[n-1]. All other polynomials span adjacent data
          points, xx[i] to xx[i+1].

            [----------][----][----][----][----------]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  n=8, degree=3
          F[0]  F[1]  F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  interpolated values

            [----------------][----][----][----------------]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  x[8]  n=9, degree=5
          F[0]  F[1]  F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  F[8]  interpolated values

        - End point derivatives: (degree - 1)/2 derivatives are specified at
          each of the xx[0] and xx[n-1] endpoints.
          The derivates are stored in
          F[0] to F[(degree - 1)/2 - 1] and
          F[n + (degree - 1)/2] to F[n + degree - 2].
          The function values are stored in
          F[(degree - 1)/2] to F[n + (degree - 1)/2 - 1].

            [----][----][----][----][----][----][----]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  n=8, degree=3
          F[1]  F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  F[8]  interpolated values
          F[0]                                      F[9]  interpolated 1st derivative

            [----][----][----][----][----][----][----][----]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  x[8]  n=9, degree=5
          F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  F[8]  F[9]  F[10] interpolated values
          F[1]                                            F[11] interpolated 1st derivative
          F[0]                                            F[12] interpolated 2nd derivative

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( degree >= 1 )
        assert( degree % 2 == 1 )
        assert( smooth >= 0 )

        nx = len( xx )
        self.nx = nx
        self.degree = degree
        self.smooth = smooth

        if (F.shape[0] == nx):
            # not-a-knot
            self.tx = notaknot_bc_knots( xx, degree )
            self.Ex = notaknot_bc_evalE( xx, self.tx, degree )
        elif (F.shape[0] == nx + degree - 1):
            # derivatives at end points
            self.tx = derivative_bc_knots( xx, degree )
            self.Ex = derivative_bc_evalE( xx, self.tx, degree )
        else:
            raise Exception("F size (%d) must be of length len(xx) = %d for not-a-knot b.c.,"
                          + " or len(xx) + degree - 1 = %d for derivative b.c."
                          % (F.shape[0], nx, nx + degree - 1))
        # end

        #-----
        # As smooth -> 0, p -> inf, we get interpolating spline.
        ## C++: compute_coeff_interpolate( Ex_, F, C_ ):
        ##     Ax_ = Ex
        ##     C = F
        ##     solve( Ax_, C )
        self.C = numpy.linalg.solve( self.Ex, F )
        Sp3 = 0.
        p3  = inf
        if (smooth == 0):
            self.p = p3
            return
        # end

        #-----
        # For smooth -> inf, p -> 0, we get least squares polynomial,
        # i.e., spline with no interior knots.
        tx_poly = only_boundary_knots( xx, degree )
        if (F.shape[0] == nx):
            Ex_poly = notaknot_bc_evalE( xx, tx_poly, degree )
        else:
            Ex_poly = deriv_bc_evalE( xx, tx_poly, degree )

        ## C++: compute_coeff_leastsquarses( Ex_poly, F, self.C, self.D )
        ##     Ax = Ex
        ##     D = F
        ##     leastsquares( Ax, D )
        ##     C = D( 0, Ex.cols() )
        (self.C, res, rank, sval) = numpy.linalg.lstsq( Ex_poly, F, rcond=None )
        Sp1 = self.residual( Ex_poly, F, self.C )
        p1  = 0.
        #print( 'lstsq', Sp1, p1 )

        # Check if smoothing condition is satisfied.
        if (Sp1 < smooth):
            print( 'worst spline (least squares) satisfies smoothing condition:',
                   'smoothing S is too large. Sp1(%.3g)=%.3g < smooth=%.3g' %
                   (p1, Sp1, smooth) )
            self.tx = tx_poly
            self.p = p1
            self.S = Sp1
            return
        # end

        #-----
        # Form remaining matrices.
        # (C++ uses Ax_ to store Hx.)
        self.Hx = evalH( self.tx, degree )
        self.HTHx = dot( self.Hx.T, self.Hx )
        self.ETEx = dot( self.Ex.T, self.Ex )

        #-----
        # Find p such that Sp = smooth.
        p2 = first_p( Sp1, Sp3, smooth )
        for iter in range( max_iters ):
            ## C++: compute_coeff_smooth( F, C, p )
            ##     Ax = ETEx + 1/p*HTHx
            ##     mul( Ex, 't', F, C )
            ##     solve( Ax, C )
            Ax = self.ETEx + (1/p2)*self.HTHx
            RHS = dot( self.Ex.T, F )
            self.C = numpy.linalg.solve( Ax, RHS )
            Sp2 = self.residual( self.Ex, F, self.C )
            #print( 'iter', iter, 'Sp2', Sp2, 'p2', p2 )

            # Verify Sp1 > Sp2 > Sp3.
            # Theoretically always true, but fails if p gets too small,
            # causing bad conditioning.
            # (C++ sets p_ = p2 and breaks loop.)
            assert(Sp1 > Sp2 and Sp2 > Sp3)

            # Check convergence.
            if (abs(Sp2 - smooth) / smooth < spline_tol):
                self.p = p2
                break
            # end

            # Update interval.
            (Sp1, p1, Sp3, p3, p2) = next_p( Sp1, Sp2, Sp3, p1, p2, p3, smooth )
        # end
    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, dx=0 ):
        '''
        Evaluate the spline or its derivatives at points in xx.

        @param[in] xx
            Array of data points to evaluate at.

        @param[in] dx
            Derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            Array of data values for spline evaluated at xx.
        '''
        nx = len( xx )
        F = numpy.zeros(( nx ))
        for i in range( nx ):
            (Ni_x, ix) = eval_basis( xx[i], self.tx, self.degree, dx )
            F[i] = numpy.dot( Ni_x,
                              self.C[ ix - self.degree : ix + 1 ] )
        # end
        return F
    # end eval

    # --------------------------------------------------------------------------
    # compute residual
    # R = Ex C - F
    # residual = |R|_fro^2
    def residual( self, Ex, F, C ):
        #print( 'Ex', Ex.shape )
        #print( 'F', F.shape )
        #print( 'C', C.shape )

        R = dot( Ex, C )
        R -= F
        nrm = norm( R )
        return nrm*nrm
    # end
# end Spline

# ==============================================================================
class Spline2D:
    # --------------------------------------------------------------------------
    def __init__( self, xx, yy, F, degree=3, smooth=0 ):
        '''
        Constructs an interpolating or smoothing spline of the given degree.

        @param[in] xx
            Array of data points in x direction, of length nx.

        @param[in] yy
            Array of data points in y direction, of length ny.

        @param[in] F
            2D array of data values.
            For not-a-knot boundary condition, of size nx-by-ny.
            For derivative boundary condition, of size
            (nx + degree - 1)-by-(ny + degree - 1).

        @param[in] degree
            Degree of the spline. Default is 3, cubic. degree >= 1 and odd.

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        Two kinds of boundary conditions are supported:
        - Not-a-knot
        - Edge derivatives

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( degree >= 1 )
        assert( degree % 2 == 1 )
        assert( smooth >= 0 )

        nx = len( xx )
        ny = len( yy )
        self.nx = nx
        self.ny = ny
        self.degree = degree
        self.smooth = smooth

        if (F.shape[0] == nx and
            F.shape[1] == ny):
            # not-a-knot
            self.tx = notaknot_bc_knots( xx, degree )
            self.ty = notaknot_bc_knots( yy, degree )
            self.Ex = notaknot_bc_evalE( xx, self.tx, degree )
            self.Ey = notaknot_bc_evalE( yy, self.ty, degree )
        elif (F.shape[0] == nx + degree - 1 and
              F.shape[1] == ny + degree - 1):
            # derivatives at edges
            self.tx = derivative_bc_knots( xx, degree )
            self.ty = derivative_bc_knots( yy, degree )
            self.Ex = derivative_bc_evalE( xx, self.tx, degree )
            self.Ey = derivative_bc_evalE( yy, self.ty, degree )
        else:
            raise Exception("F must be of size len(xx)-by-len(yy) or\n" +
                            "(len(xx) + degree - 1)-by-(len(yy) + degree - 1)")

        # for smooth = 0, p = inf, we get interpolating spline
        Cx = numpy.linalg.solve( self.Ex, F )
        Cy = numpy.linalg.solve( self.Ey, Cx.T )
        self.C = Cy.T
        self.p = inf
        if (smooth == 0):
            return

        assert( false )

        # for smooth -> inf, p -> 0, we get least squares polynomial
        # i.e., spline with no interior knots
        #self.tx_poly = only_boundary_knots( xx, degree )

    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, yy, dx=0, dy=0 ):
        '''
        Evaluate the spline or its derivatives at points in the Cartesian
        product of (xx, yy).

        @param[in] xx
            Array of data points to evaluate at in x direction.

        @param[in] yy
            Array of data points to evaluate at in y direction.

        @param[in] dx
            x derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dy
            y derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            2D array of data values for spline evaluated at (xx, yy).
        '''
        d = self.degree
        nx = len( xx )
        ny = len( yy )
        F = numpy.zeros(( nx, ny ))
        for j in range( ny ):
            (Ni_y, iy) = eval_basis( yy[j], self.ty, d, dy )

            for i in range( nx ):
                (Ni_x, ix) = eval_basis( xx[i], self.tx, d, dx )

                # Either notation works:
                # F[ i, j ] = numpy.sum( numpy.outer( Ni_x, Ni_y ) *
                #                        self.C[ ix - d : ix + 1,
                #                                iy - d : iy + 1 ] )
                # Or:
                F[ i, j ] = numpy.dot( Ni_y,
                            numpy.dot( Ni_x,
                                       self.C[ ix - d : ix + 1,
                                               iy - d : iy + 1 ] ))
            # end
        # end
        return F
    # end eval
# end Spline2D

# ==============================================================================
# 3D utilities

# ------------------------------------------------------------------------------
def solve3D( A, B ):
    '''
    Solves $x = Ab$ for each "column"  of the 3D array B.
    Columns are along the 1st dimension, so the (j, k)-th column is B[:, j, k].
    '''
    (nx, ny, nz) = B.shape
    X = numpy.linalg.solve( A, B.reshape(( nx, ny*nz )) )
    X = X.reshape(( nx, ny, nz ))

    # Functionally the same as:
    # X2 = numpy.zeros(( nx, ny, nz ))
    # for j in range( ny ):
    #     for k in range( nz ):
    #         b = B[ :, j, k ]
    #         X2[ :, j, k ] = numpy.linalg.solve( A, b )
    # print( "solve3D  error %.2e" % (norm( X2 - X )) )

    return X
# end

# ------------------------------------------------------------------------------
def rotate3D( F ):
    r'''
    Applies a cyclic permutation to the indices of $F$: $i, j, k \to j, k, i$.
    '''
    G = numpy.transpose( F, (1, 2, 0) )

    # Functionally the same as:
    # (nx, ny, nz) = F.shape
    # G2 = numpy.zeros(( ny, nz, nx ))
    # for k in range( nz ):
    #     for j in range( ny ):
    #         for i in range( nx ):
    #             G2[ j, k, i ] = F[ i, j, k ]
    # print( "rotate3D error %.2e" % (norm( G2 - G )) )

    return G
# end

# ==============================================================================
class Spline3D:
    # --------------------------------------------------------------------------
    def __init__( self, xx, yy, zz, F, degree=3, smooth=0 ):
        '''
        Constructs an interpolating or smoothing spline of the given degree.

        @param[in] xx
            Array of data points in x direction, of length nx.

        @param[in] yy
            Array of data points in y direction, of length ny.

        @param[in] zz
            Array of data points in z direction, of length nz.

        @param[in] F
            3D array of data values.
            For not-a-knot boundary condition, of size nx-by-ny-by-nz.
            For derivative boundary condition, of size
            (nx + degree - 1)-by-(ny + degree - 1).

        @param[in] degree
            Degree of the spline. Default is 3, cubic. degree >= 1 and odd.

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        Two kinds of boundary conditions are supported:
        - Not-a-knot
        - Edge derivatives

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( degree >= 1 )
        assert( degree % 2 == 1 )
        assert( smooth >= 0 )

        nx = len( xx )
        ny = len( yy )
        nz = len( zz )
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.degree = degree
        self.smooth = smooth

        if (F.shape[0] == nx and
            F.shape[1] == ny and
            F.shape[2] == nz):
            # not-a-knot
            self.tx = notaknot_bc_knots( xx, degree )
            self.ty = notaknot_bc_knots( yy, degree )
            self.tz = notaknot_bc_knots( zz, degree )
            self.Ex = notaknot_bc_evalE( xx, self.tx, degree )
            self.Ey = notaknot_bc_evalE( yy, self.ty, degree )
            self.Ez = notaknot_bc_evalE( zz, self.tz, degree )
        elif (F.shape[0] == nx + degree - 1 and
              F.shape[1] == ny + degree - 1 and
              F.shape[2] == nz + degree - 1):
            # derivatives at edges
            self.tx = derivative_bc_knots( xx, degree )
            self.ty = derivative_bc_knots( yy, degree )
            self.tz = derivative_bc_knots( zz, degree )
            self.Ex = derivative_bc_evalE( xx, self.tx, degree )
            self.Ey = derivative_bc_evalE( yy, self.ty, degree )
            self.Ez = derivative_bc_evalE( zz, self.tz, degree )
        else:
            raise Exception("F must be of size len(xx)-by-len(yy)-by-len(zz) or\n" +
                            "(len(xx) + degree - 1)-by-(len(yy) + degree - 1)-by-(len(zz) + degree - 1)")

        # for smooth = 0, p = inf, we get interpolating spline
        # numpy doesn't understand solving a 3D array,
        # so solve3D handles reshaping the array as needed.
        # Apply cyclic permutation between each step via rotate3D,
        # from xyz -> yzx -> zxy -> xyz.
        Cxyz = solve3D( self.Ex, F )
        Cyzx = rotate3D( Cxyz )

        Cyzx = solve3D( self.Ey, Cyzx )
        Czxy = rotate3D( Cyzx )

        Czxy = solve3D( self.Ez, Czxy )
        self.C = rotate3D( Czxy ) # back to xyz order

        self.p = inf
        if (smooth == 0):
            return

        assert( false )

        # for smooth -> inf, p -> 0, we get least squares polynomial
        # i.e., spline with no interior knots
        #self.tx_poly = only_boundary_knots( xx, degree )

    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, yy, zz, dx=0, dy=0, dz=0 ):
        '''
        Evaluate the spline or its derivatives at points in the Cartesian
        product of (xx, yy, zz).

        @param[in] xx
            Array of data points to evaluate at in x direction.

        @param[in] yy
            Array of data points to evaluate at in y direction.

        @param[in] zz
            Array of data points to evaluate at in z direction.

        @param[in] dx
            x derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dy
            y derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dz
            z derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            3D array of data values for spline evaluated at (xx, yy, zz).
        '''
        d = self.degree
        nx = len( xx )
        ny = len( yy )
        nz = len( zz )
        F = numpy.zeros(( nx, ny, nz ))
        for k in range( nz ):
            (Ni_z, iz) = eval_basis( zz[k], self.tz, d, dz )

            for j in range( ny ):
                (Ni_y, iy) = eval_basis( yy[j], self.ty, d, dy )

                for i in range( nx ):
                    (Ni_x, ix) = eval_basis( xx[i], self.tx, d, dx )

                    # numpy contracts higher order tensors in a weird order.
                    # Use reshape to make it 2D so the contraction works.
                    C = self.C[ ix - d : ix + 1,
                                iy - d : iy + 1,
                                iz - d : iz + 1 ]
                    F[ i, j, k ] = numpy.dot( Ni_z,
                                   numpy.dot( Ni_y,
                                   numpy.dot( Ni_x, C.reshape( 4, 16 ) ).reshape( 4, 4 )))

                    # Functionally the same as:
                    # Fsum = 0
                    # for kk in range(4):
                    #     for jj in range(4):
                    #         for ii in range(4):
                    #             Fsum += (Ni_x[ii] * Ni_y[jj] * Ni_z[kk]
                    #                       * self.C[ ix-d+ii, iy-d+jj, iz-d+kk ])
                    # assert( abs( F[i,j,k] - Fsum ) < 1e-14 )
                    #print( 'i %3d, j %3d, k %3d, F %7.4f, F2 %7.4f, diff %9.2e' %
                    #       (i, j, k, F[i,j,k], Fsum, F[i,j,k] - Fsum) )
                # end
            # end
        # end
        return F
    # end eval
# end Spline3D

# ==============================================================================
# cubic uniform utilities

# ------------------------------------------------------------------------------
def cubic_uniform_eval_basis( xt0 ):
    '''
    Evaluate cubic b-spline basis functions at xt0, assuming unit spaced knots.
    '''
    xt1 = xt0 + 1.
    xt2 = xt0 + 2.

    tx0 = 1. - xt0
    tx1 = tx0 + 1.
    tx2 = tx0 + 2.

    Ni_1_2 = tx0
    Ni_0_2 = xt0

    Ni_2_3 = 0.5*(              tx0*Ni_1_2 )
    Ni_1_3 = 0.5*( xt1*Ni_1_2 + tx1*Ni_0_2 )
    Ni_0_3 = 0.5*( xt0*Ni_0_2              )

    Ni = (1/3.) * numpy.array((
                     tx0*Ni_2_3,
        xt2*Ni_2_3 + tx1*Ni_1_3,
        xt1*Ni_1_3 + tx2*Ni_0_3,
        xt0*Ni_0_3
    ))
    return Ni
# end

# ------------------------------------------------------------------------------
def cubic_uniform_eval_basis_dx( xt0 ):
    '''
    Evaluate cubic b-spline basis functions and 1st derivatives at xt0,
    assuming unit spaced knots.
    '''
    xt1 = xt0 + 1.
    xt2 = xt0 + 2.

    tx0 = 1. - xt0
    tx1 = tx0 + 1.
    tx2 = tx0 + 2.

    Ni_1_2 = tx0
    Ni_0_2 = xt0

    Ni_2_3 = 0.5*(              tx0*Ni_1_2 )
    Ni_1_3 = 0.5*( xt1*Ni_1_2 + tx1*Ni_0_2 )
    Ni_0_3 = 0.5*( xt0*Ni_0_2              )

    Ni = (1/3.) * numpy.array((
                     tx0*Ni_2_3,
        xt2*Ni_2_3 + tx1*Ni_1_3,
        xt1*Ni_1_3 + tx2*Ni_0_3,
        xt0*Ni_0_3
    ))
    # above code same as in cubic_uniform_eval_basis()

    # ----------
    dNi = numpy.array((
               - Ni_2_3,
        Ni_2_3 - Ni_1_3,
        Ni_1_3 - Ni_0_3,
        Ni_0_3
    ))

    return (Ni, dNi)
# end

# ------------------------------------------------------------------------------
def cubic_uniform_eval_basis_dx2( xt0 ):
    '''
    Evaluate cubic b-spline basis functions, 1st and 2nd derivatives at xt0,
    assuming unit spaced knots.
    '''
    xt1 = xt0 + 1.
    xt2 = xt0 + 2.

    tx0 = 1. - xt0
    tx1 = tx0 + 1.
    tx2 = tx0 + 2.

    Ni_1_2 = tx0
    Ni_0_2 = xt0

    Ni_2_3 = 0.5*(              tx0*Ni_1_2 )
    Ni_1_3 = 0.5*( xt1*Ni_1_2 + tx1*Ni_0_2 )
    Ni_0_3 = 0.5*( xt0*Ni_0_2              )

    Ni = (1/3.) * numpy.array((
                     tx0*Ni_2_3,
        xt2*Ni_2_3 + tx1*Ni_1_3,
        xt1*Ni_1_3 + tx2*Ni_0_3,
        xt0*Ni_0_3
    ))
    # above code same as in cubic_uniform_eval_basis()

    # ----------
    dNi = numpy.array((
               - Ni_2_3,
        Ni_2_3 - Ni_1_3,
        Ni_1_3 - Ni_0_3,
        Ni_0_3
    ))
    # above code same as in cubic_uniform_eval_basis_dx()

    # ----------
    dNi_2_3 =        - Ni_1_2   # d/dx N{i-2,3}
    dNi_1_3 = Ni_1_2 - Ni_0_2   # d/dx N{i-1,3}
    dNi_0_3 = Ni_0_2            # d/dx N{i  ,3}

    d2Ni = numpy.array((
                - dNi_2_3,      # d^2/dx^2 N{i-3,4}
        dNi_2_3 - dNi_1_3,      # d^2/dx^2 N{i-2,4}
        dNi_1_3 - dNi_0_3,      # d^2/dx^2 N{i-1,4}
        dNi_0_3                 # d^2/dx^2 N{i  ,4}
    ))
    return (Ni, dNi, d2Ni)
# end

# ------------------------------------------------------------------------------
def cubic_uniform_evalE( nx ):
    '''
    Generates the matrix E that enforces interpolation at data points,
    assuming a cubic spline with data at uniform unit-spaced knots.
    To determine the spline coefficients $C$, solve
        \[ E C = F \],
    where $F$ is function values corresponding to data points in $x$.

        [ N0(x0)'      ...  N{nt-5}(x0)'      ]  <= derivative at endpoint
        [ N0(x0)       ...  N{nt-5}(x0)       ]
        [ N0(x1)       ...  N{nt-5}(x1)       ]
    E = [              ...                    ]
        [ N0(x{n-2})   ...  N{nt-5}(x{nx-2})  ]
        [ N0(x{n-1})   ...  N{nt-5}(x{nx-1})  ]
        [ N0(x{n-1})'  ...  N{nt-5}(x{nx-1})' ]  <= derivative at endpoint

        [ -1/2  0    1/2            ]
        [  1/6  2/3  1/6            ]
      = [        .    .    .        ]  for data at unit spaced knots
        [            1/6  2/3  1/6  ]
        [           -1/2  0    1/2  ]

    @param[in] nx
        Number of data values, including 1st derivatives at endpoints.

    @return
        Matrix E, of size (n + 2)-by-(n + 2).
    '''
    (Ni, dNi) = cubic_uniform_eval_basis_dx( 0.0 )
    E = numpy.zeros(( nx+2, nx+2 ))

    # left boundary derivative
    E[ 0, 0 ] = dNi[0]
    E[ 0, 1 ] = dNi[1]
    E[ 0, 2 ] = dNi[2]

    # interior knots
    for i in range( 1, nx+1 ):
        E[ i, i-1 ] = Ni[0]
        E[ i, i   ] = Ni[1]
        E[ i, i+1 ] = Ni[2]
    # end

    # right boundary derivative
    E[ nx+1, nx-1 ] = dNi[0]
    E[ nx+1, nx   ] = dNi[1]
    E[ nx+1, nx+1 ] = dNi[2]

    return E
# end

# ==============================================================================
class Spline_cubic_uniform:
    # --------------------------------------------------------------------------
    def __init__( self, xmin, F, smooth=0 ):
        '''
        Constructs an interpolating or smoothing cubic spline with knots at
        uniform unit intervals.

        @param[in] xmin
            Minimum value of x coordinate.
            Knots are assumed at [xmin, xmin + 1, xmin + 2, xmin + len(F) - 1].

        @param[in] F
            Array of data values.
            For derivative boundary condition, of length n + 2.

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        A spline is a piece-wise polynomial function. All or some of the data
        points are specified as knots. Between each pair of knots, the spline
        is a cubic polynomial. At each knot, the cubic spline has C^2
        continuity.

        A spline of the given degree with n knots has (n-1)*(degree+1) degrees-of-freedom.
        (n-1)degree + (n-1) = nd + n - degree - 1

        n data points, (n-2)*degree enforced continuity at interior knots
        n + (nd - 2d) = nd + n - 2d + (degree-1)
        need (degree-1) extra derivatives.

        Only the end point derivative boundary condition is supported:
        - End point derivatives: one derivative is specified at
          each of the xx[0] and xx[n-1] endpoints.
          The derivates are stored in F[0] and F[n + 1].
          The function values are stored in F[1] to F[n].

            [----][----][----][----][----][----][----]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  n=8, degree=3
          F[1]  F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  F[8]  interpolated values
          F[0]                                      F[9]  interpolated 1st derivative

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( smooth >= 0 )

        # number of knots, which excludes derivatives at ends
        nx = len( F ) - 2
        self.nx = nx
        self.smooth = smooth
        self.xmin = xmin

        self.Ex = cubic_uniform_evalE( nx )

        # for smooth = 0, p = inf, we get interpolating spline
        self.C = numpy.linalg.solve( self.Ex, F )
        self.p = inf
        if (smooth == 0):
            return

        assert( false )

        # for smooth -> inf, p -> 0, we get least squares polynomial
        # i.e., spline with no interior knots
        #self.tx_poly = only_boundary_knots( xx, degree )

    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, dx=0 ):
        '''
        Evaluate the spline or its derivatives at points in xx.

        @param[in] xx
            Array of data points to evaluate at.

        @param[in] dx
            Derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            Array of data values for spline evaluated at xx.
        '''
        # 3rd and higher derivatives not implemented.
        # 3rd is step function.
        # 4th and higher are zero except for deltas at knots.
        assert( 0 <= dx <= 2 )

        nx = len( xx )
        F = numpy.zeros(( nx ))
        for i in range( nx ):
            ix = min( int(xx[i] - self.xmin), self.nx - 2 )  # knot's index
            tx = ix + self.xmin                              # knot's coordinate

            if   (dx == 0):
                Ni_x = cubic_uniform_eval_basis( xx[i] - tx )
            elif (dx == 1):
                (Ni_x, dNi_x) = cubic_uniform_eval_basis_dx( xx[i] - tx )
                Ni_x = dNi_x
            elif (dx == 2):
                (Ni_x, dNi_x, d2Ni_x) = cubic_uniform_eval_basis_dx2( xx[i] - tx )
                Ni_x = d2Ni_x

            F[i] = numpy.dot( Ni_x, self.C[ ix : ix + 4 ] )
        # end
        return F
    # end eval
# end Spline_cubic_uniform

# ==============================================================================
class Spline2D_cubic_uniform:
    # --------------------------------------------------------------------------
    def __init__( self, xmin, ymin, F, smooth=0 ):
        '''
        Constructs an interpolating or smoothing cubic spline with knots at
        uniform unit intervals.

        @param[in] xmin
            Minimum value of x coordinate.
            Knots are assumed at [xmin, xmin + 1, xmin + 2, xmin + nx - 1].

        @param[in] ymin
            Minimum value of y coordinate.
            Knots are assumed at [ymin, ymin + 1, ymin + 2, ymin + ny - 1].

        @param[in] F
            Array of data values.
            For derivative boundary condition,
            of size (nx + 2)-by-(ny + 2).

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        A spline is a piece-wise polynomial function. All or some of the data
        points are specified as knots. Between each pair of knots, the spline
        is a cubic polynomial. At each knot, the cubic spline has C^2
        continuity.

        A spline of the given degree with n knots has (n-1)*(degree+1) degrees-of-freedom.
        (n-1)degree + (n-1) = nd + n - degree - 1

        n data points, (n-2)*degree enforced continuity at interior knots
        n + (nd - 2d) = nd + n - 2d + (degree-1)
        need (degree-1) extra derivatives.

        Only the end point derivative boundary condition is supported:
        - End point derivatives: one derivative is specified at
          each of the xx[0] and xx[n-1] endpoints.
          The derivates are stored in F[0] and F[n + 1].
          The function values are stored in F[1] to F[n].

            [----][----][----][----][----][----][----]    polynomial spans
          x[0]  x[1]  x[2]  x[3]  x[4]  x[5]  x[6]  x[7]  n=8, degree=3
          F[1]  F[2]  F[3]  F[4]  F[5]  F[6]  F[7]  F[8]  interpolated values
          F[0]                                      F[9]  interpolated 1st derivative

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( smooth >= 0 )

        # number of knots, which excludes derivatives at ends
        (nx, ny) = F.shape
        nx -= 2
        ny -= 2
        self.nx = nx
        self.ny = ny
        self.smooth = smooth
        self.xmin = xmin
        self.ymin = ymin

        self.Ex = cubic_uniform_evalE( nx )
        self.Ey = cubic_uniform_evalE( ny )

        # for smooth = 0, p = inf, we get interpolating spline
        Cx = numpy.linalg.solve( self.Ex, F )
        Cy = numpy.linalg.solve( self.Ey, Cx.T )
        self.C = Cy.T
        self.p = inf
        if (smooth == 0):
            return

        assert( false )

        # for smooth -> inf, p -> 0, we get least squares polynomial
        # i.e., spline with no interior knots
        #self.tx_poly = only_boundary_knots( xx, degree )

    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, yy, dx=0, dy=0 ):
        '''
        Evaluate the spline or its derivatives at points in the Cartesian
        product of (xx, yy).

        @param[in] xx
            Array of data points to evaluate at in x direction.

        @param[in] yy
            Array of data points to evaluate at in y direction.

        @param[in] dx
            x derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dy
            y derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            Array of data values for spline evaluated at (xx, yy).
        '''
        # 3rd and higher derivatives not implemented.
        # 3rd is step function.
        # 4th and higher are zero except for deltas at knots.
        assert( 0 <= dx <= 2 )
        assert( 0 <= dy <= 2 )

        nx = len( xx )
        ny = len( yy )
        F = numpy.zeros(( nx, ny ))
        for j in range( ny ):
            iy = min( int(yy[j] - self.ymin), self.ny - 2 )  # knot's index
            ty = iy + self.ymin                              # knot's coordinate

            if   (dy == 0):
                Ni_y = cubic_uniform_eval_basis( yy[j] - ty )
            elif (dy == 1):
                (Ni_y, dNi_y) = cubic_uniform_eval_basis_dx( yy[j] - ty )
                Ni_y = dNi_y
            elif (dy == 2):
                (Ni_y, dNi_y, d2Ni_y) = cubic_uniform_eval_basis_dx2( yy[j] - ty )
                Ni_y = d2Ni_y

            for i in range( nx ):
                ix = min( int(xx[i] - self.xmin), self.nx - 2 )  # knot's index
                tx = ix + self.xmin                              # knot's coordinate

                if   (dx == 0):
                    Ni_x = cubic_uniform_eval_basis( xx[i] - tx )
                elif (dx == 1):
                    (Ni_x, dNi_x) = cubic_uniform_eval_basis_dx( xx[i] - tx )
                    Ni_x = dNi_x
                elif (dx == 2):
                    (Ni_x, dNi_x, d2Ni_x) = cubic_uniform_eval_basis_dx2( xx[i] - tx )
                    Ni_x = d2Ni_x

                # Either notation works:
                # F[ i, j ] = numpy.sum( numpy.outer( Ni_x, Ni_y ) *
                #                        self.C[ ix - d : ix + 1,
                #                                iy - d : iy + 1 ] )
                # Or:
                F[ i, j ] = numpy.dot( Ni_y,
                            numpy.dot( Ni_x,
                                       self.C[ ix : ix + 4,
                                               iy : iy + 4 ] ))
            # end
        # end
        return F
    # end eval
# end Spline2D_cubic_uniform

# ==============================================================================
class Spline3D_cubic_uniform:
    # --------------------------------------------------------------------------
    def __init__( self, xmin, ymin, zmin, F, smooth=0 ):
        '''
        Constructs an interpolating or smoothing cubic spline with knots at
        uniform unit intervals.

        @param[in] xmin
            Minimum value of x coordinate.
            Knots are assumed at [xmin, xmin + 1, xmin + 2, xmin + nx - 1].

        @param[in] ymin
            Minimum value of y coordinate.
            Knots are assumed at [ymin, ymin + 1, ymin + 2, ymin + ny - 1].

        @param[in] zmin
            Minimum value of z coordinate.
            Knots are assumed at [zmin, zmin + 1, zmin + 2, zmin + nz - 1].

        @param[in] F
            Array of data values.
            For derivative boundary condition,
            of size (nx + 2)-by-(ny + 2)-by-(nz + 2).

        @param[in] smooth
            Smoothing factor. Default is 0, no smoothing. smooth >= 0.

        With a smoothing spline, all data values are approximated instead of
        interpolated.
        '''
        assert( smooth >= 0 )

        # number of knots, which excludes derivatives at ends
        (nx, ny, nz) = F.shape
        nx -= 2
        ny -= 2
        nz -= 2
        self.nx = nx
        self.ny = ny
        self.nz = nz
        self.smooth = smooth
        self.xmin = xmin
        self.ymin = ymin
        self.zmin = zmin

        self.Ex = cubic_uniform_evalE( nx )
        self.Ey = cubic_uniform_evalE( ny )
        self.Ez = cubic_uniform_evalE( nz )

        # for smooth = 0, p = inf, we get interpolating spline
        Cxyz = solve3D( self.Ex, F )
        Cyzx = rotate3D( Cxyz )
        Cyzx = solve3D( self.Ey, Cyzx )
        Czxy = rotate3D( Cyzx )
        Czxy = solve3D( self.Ez, Czxy )
        self.C = rotate3D( Czxy ) # back to xyz order
        self.p = inf
        if (smooth == 0):
            return

        assert( false )

        # for smooth -> inf, p -> 0, we get least squares polynomial
        # i.e., spline with no interior knots
        #self.tx_poly = only_boundary_knots( xx, degree )

    # end init

    # --------------------------------------------------------------------------
    def __call__( self, xx, yy, zz, dx=0, dy=0, dz=0 ):
        '''
        Evaluate the spline or its derivatives at points in the Cartesian
        product of (xx, yy, zz).

        @param[in] xx
            Array of data points to evaluate at in x direction.

        @param[in] yy
            Array of data points to evaluate at in y direction.

        @param[in] zz
            Array of data points to evaluate at in z direction.

        @param[in] dx
            x derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dy
            y derivative to evaluate. Default is 0, i.e., the spline itself.

        @param[in] dz
            z derivative to evaluate. Default is 0, i.e., the spline itself.

        @return
            Array of data values for spline evaluated at (xx, yy, zz).
        '''
        # 3rd and higher derivatives not implemented.
        # 3rd is step function.
        # 4th and higher are zero except for deltas at knots.
        assert( 0 <= dx <= 2 )
        assert( 0 <= dy <= 2 )
        assert( 0 <= dz <= 2 )

        nx = len( xx )
        ny = len( yy )
        nz = len( zz )
        F = numpy.zeros(( nx, ny, nz ))
        for k in range( nz ):
            iz = min( int(zz[k] - self.zmin), self.nz - 2 )  # knot's index
            tz = iz + self.zmin                              # knot's coordinate

            if   (dz == 0):
                Ni_z = cubic_uniform_eval_basis( zz[k] - tz )
            elif (dz == 1):
                (Ni_z, dNi_z) = cubic_uniform_eval_basis_dx( zz[k] - tz )
                Ni_z = dNi_z
            elif (dz == 2):
                (Ni_z, dNi_z, d2Ni_z) = cubic_uniform_eval_basis_dx2( zz[k] - tz )
                Ni_z = d2Ni_z

            for j in range( ny ):
                iy = min( int(yy[j] - self.ymin), self.ny - 2 )  # knot's index
                ty = iy + self.ymin                              # knot's coordinate

                if   (dy == 0):
                    Ni_y = cubic_uniform_eval_basis( yy[j] - ty )
                elif (dy == 1):
                    (Ni_y, dNi_y) = cubic_uniform_eval_basis_dx( yy[j] - ty )
                    Ni_y = dNi_y
                elif (dy == 2):
                    (Ni_y, dNi_y, d2Ni_y) = cubic_uniform_eval_basis_dx2( yy[j] - ty )
                    Ni_y = d2Ni_y

                for i in range( nx ):
                    ix = min( int(xx[i] - self.xmin), self.nx - 2 )  # knot's index
                    tx = ix + self.xmin                              # knot's coordinate

                    if   (dx == 0):
                        Ni_x = cubic_uniform_eval_basis( xx[i] - tx )
                    elif (dx == 1):
                        (Ni_x, dNi_x) = cubic_uniform_eval_basis_dx( xx[i] - tx )
                        Ni_x = dNi_x
                    elif (dx == 2):
                        (Ni_x, dNi_x, d2Ni_x) = cubic_uniform_eval_basis_dx2( xx[i] - tx )
                        Ni_x = d2Ni_x

                    # numpy contracts higher order tensors in a weird order.
                    # Use reshape to make it 2D so the contraction works.
                    C = self.C[ ix : ix + 4,
                                iy : iy + 4,
                                iz : iz + 4 ]
                    F[ i, j, k ] = numpy.dot( Ni_z,
                                   numpy.dot( Ni_y,
                                   numpy.dot( Ni_x, C.reshape( 4, 16 ) ).reshape( 4, 4 )))
                # end
            # end
        # end
        return F
    # end eval
# end Spline3D_cubic_uniform
